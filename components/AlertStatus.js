import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { TextFont } from './text';

export default class AlertStatus extends React.Component {
  render() {
    return (
      <View style={style.container}>
        <TouchableOpacity onPress={this.props.onPressAction}>
          <TextFont fontSize={16} color="#ffffff">
            <TextFont>{this.props.textHelper} </TextFont>
            <TextFont fontWeight="700">{this.props.textAction}</TextFont>
          </TextFont>
        </TouchableOpacity>
      </View>
    );
  }
}

AlertStatus.propTypes = {
  textHelper: PropTypes.string,
  textAction: PropTypes.string,
  onPressAction: PropTypes.func,
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    backgroundColor: 'rgba(45, 54, 71, 0.6)',
    bottom: 0,
    right: 0,
    left: 0,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
