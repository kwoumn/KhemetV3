import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';
import { getStyleFromProps } from '../utils';

export default class Logo extends React.Component {
  render() {
    const style = [
      logoStyle.imageContainer,
      getStyleFromProps(['marginTop'], this.props),
    ];
    return (
      <View style={style}>
        <Image
          source={require('../images/khemet_logo.png')}
          style={logoStyle.image}
          resizeMode="cover"
        />
      </View>
    );
  }
}

Logo.propTypes = {
  marginTop: PropTypes.number,
};

const logoStyle = StyleSheet.create({
  imageContainer: {
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
  },
});
