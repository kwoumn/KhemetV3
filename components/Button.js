import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { getStyleFromProps } from '../utils';
import { TextFont } from './text';

export default class Button extends React.Component {
  render() {
    const style = {
      ...styleButton.container,
      ...getStyleFromProps(
        ['marginTop', 'width', 'flex', 'backgroundColor'],
        this.props
      ),
    };

    return (
      <View style={{ alignItems: 'center' }}>
        <TouchableOpacity style={style} onPress={this.props.onPress}>
          <TextFont style={styleButton.text}>{this.props.children}</TextFont>
        </TouchableOpacity>
      </View>
    );
  }
}

Button.defaultProps = {};

Button.propTypes = {
  marginTop: PropTypes.number,
  width: PropTypes.number,
  flex: PropTypes.number,
  onPress: PropTypes.func,
  backgroundColor: PropTypes.string,
};

const styleButton = {
  container: {
    paddingTop: 16,
    paddingBottom: 16,
    paddingLeft: 47,
    paddingRight: 47,
    backgroundColor: '#2d3647',
    borderRadius: 30,
    alignItems: 'stretch',
    shadowColor: '#000000',
    shadowOpacity: 0.5,
    shadowRadius: 2,
    shadowOffset: {
      height: 2,
      width: 1,
    },
    flexDirection: 'row',
  },
  text: {
    color: '#ffffff',
    fontSize: 18,
    fontWeight: '600',
    textAlign: 'center',
    letterSpacing: 3,
    flex: 1,
    flexWrap: 'wrap',
  },
};
