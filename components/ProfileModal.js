import React from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import Modal from 'react-native-modal';
import { Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  organizationToGreekLettersMap,
  formatLineNumber,
} from '../api/constants';
import * as locationActions from '../actions/locationActions';
import * as userActions from '../actions/userActions';

class ProfileModal extends React.Component {
  constructor() {
    super();
    this.onPressMessage = this.onPressMessage.bind(this);
    this.handleBlockToggle = this.handleBlockToggle.bind(this);
  }

  onPressMessage() {
    this.props.setIsProfileModalVisible(false);
    this.props.navigation.navigate('chat', {
      recipientName: this.props.selectedUser.firstName,
      showActionSheet: this.props.showActionSheet,
    });
  }

  handleBlockToggle(currentUserUid, blockedUserUid) {
    this.props.blockedUsers.includes(this.props.selectedUser.uid)
      ? this.props.unblockUser(currentUserUid, blockedUserUid)
      : this.props.blockUser(currentUserUid, blockedUserUid);
  }

  render() {
    const {
      selectedUser,
      blockedUsers,
      blockUser,
      currentUserUid,
      showActionSheet,
    } = this.props;
    const isBlocked = blockedUsers.includes(selectedUser.uid);
    const paddingRight = isBlocked ? 0 : 25;
    const profilePhotoSource =
      selectedUser.profilePhotoUrl && selectedUser.profilePhotoUrl !== ''
        ? {
            isStatic: true,
            uri: selectedUser.profilePhotoUrl,
          }
        : {
            isStatic: true,
          };
    return (
      <View style={styles.container}>
        <Modal isVisible={this.props.isProfileModalVisible}>
          <Card>
            <View style={styles.modalIcons}>
              <View>
                <TouchableOpacity
                  onPress={() => this.props.setIsProfileModalVisible(false)}
                >
                  <Icon name="close-box" size={30} />
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity onPress={showActionSheet}>
                  <Icon name="dots-horizontal" size={30} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.modalContent}>
              <View style={styles.headerColumn}>
                <Image style={styles.userImage} source={profilePhotoSource} />
                <Text style={styles.userNameText}>{`${
                  selectedUser.firstName
                }`}</Text>
                <Text style={styles.otherText}>
                  {organizationToGreekLettersMap[selectedUser.organization]}
                </Text>
                <Text style={styles.otherText}>{selectedUser.chapter}</Text>
                <Text style={styles.otherText}>{selectedUser.school}</Text>
                <Text style={styles.otherText}>{`${
                  selectedUser.crossingSeason
                } ${selectedUser.crossingYear} / ${formatLineNumber(
                  selectedUser.lineNumber
                )} Club`}</Text>
                <Text style={styles.otherText}>{selectedUser.occupation}</Text>
                <View style={{ flexDirection: 'row' }}>
                  <TouchableOpacity
                    onPress={() =>
                      this.handleBlockToggle(currentUserUid, selectedUser.uid)
                    }
                    style={{ paddingRight: paddingRight }}
                  >
                    <Icon name="block-helper" size={40} color="#FF0000" />
                    {!isBlocked ? <Text>Block</Text> : <Text>Unblock</Text>}
                  </TouchableOpacity>
                  {!isBlocked && (
                    <TouchableOpacity onPress={this.onPressMessage}>
                      <Icon name="message-text" size={40} color="#007AFF" />
                      <Text>Message</Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </View>
          </Card>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  currentUserUid: state.user.user.uid,
  isProfileModalVisible: state.user.isProfileModalVisible,
  selectedUser: state.user.selectedUser,
  blockedUsers: state.user.blocked,
});

function mapDispatchToProps(dispatch, ownProps) {
  return {
    setIsProfileModalVisible: isProfileModalVisible =>
      dispatch(userActions.setIsProfileModalVisible(isProfileModalVisible)),
    blockUser: (currentUserUid, blockedUserUid) =>
      dispatch(userActions.blockUser(currentUserUid, blockedUserUid)),
    unblockUser: (currentUserUid, unblockedUserUid) =>
      dispatch(userActions.unblockUser(currentUserUid, unblockedUserUid)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerColumn: {
    backgroundColor: 'transparent',
    ...Platform.select({
      ios: {
        alignItems: 'center',
        elevation: 1,
        marginTop: -1,
      },
      android: {
        alignItems: 'center',
      },
    }),
  },
  userImage: {
    borderColor: '#000',
    borderRadius: 85,
    borderWidth: 3,
    height: 170,
    marginBottom: 15,
    width: 170,
  },
  userNameText: {
    color: '#000000',
    fontSize: 22,
    fontWeight: 'bold',
    paddingBottom: 8,
    textAlign: 'center',
  },
  otherText: {
    color: '#000000',
    fontSize: 18,
    paddingBottom: 8,
    textAlign: 'center',
  },
  modalContent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  closeIcon: {
    alignItems: 'flex-end',
  },
  modalIcons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
