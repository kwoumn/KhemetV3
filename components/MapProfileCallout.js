import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { Avatar, Card } from 'react-native-elements';
import * as userActions from '../actions/userActions';
import * as locationActions from '../actions/locationActions';

class MapProfileCallout extends React.Component {
  constructor() {
    super();
    this.state = {
      firstName: '',
      lastName: '',
      organization: '',
      chapter: '',
      lineNumber: '',
      school: '',
    };
    this.onPressMapProfileCallout = this.onPressMapProfileCallout.bind(this);
    this.contactCardEvent = this.contactCardEvent.bind(this);
  }

  componentDidMount() {
    this.props.getUserInfoFromUid().then(val => {
      this.setState({
        uid: this.props.selectedUserUid,
        firstName: val.firstName,
        lastName: val.lastName,
        organization: val.organization,
        chapter: val.chapter,
        lineNumber: val.lineNumber,
        occupation: val.occupation,
        school: val.school,
        crossingSeason: val.crossingSeason,
        crossingYear: val.crossingYear,
        profilePhotoUrl: val.profilePhotoUrl,
      });
    });
  }

  onPressMapProfileCallout() {
    this.props.setSelectedUser(this.state);
    this.props.setIsProfileModalVisible(!this.props.isProfileModalVisible);
    this.contactCardEvent();
  }

  contactCardEvent() {
    const { uid, location, authenticatedUser } = this.props;
    const selectedUser = this.state;
    const contactCardEvent = 'contact_card';
    const currentUserLongitude = location.longitude;
    const currentUserLatitude = location.latitude;
    const userProperties = {
      organization: authenticatedUser.organization,
      chapter: authenticatedUser.chapter,
      school: authenticatedUser.school,
      crossingSeason: authenticatedUser.crossingSeason,
      crossingYear: authenticatedUser.crossingYear,
      lineNumber: authenticatedUser.lineNumber,
    };
    const eventProperties = {
      contactSelectedUserId: selectedUser.uid,
      contactSelectedOrganization: selectedUser.organization,
      contactSelectedOccupaton: selectedUser.occupation,
      contactSelectedLatitude: location.locationsToDisplay.find(
        locationObj => locationObj.uid === selectedUser.uid
      ).latitude,
      contactSelectedLongitude: location.locationsToDisplay.find(
        locationObj => locationObj.uid === selectedUser.uid
      ).longitude,
    };
    const queryParams = {
      longitude: currentUserLongitude,
      latitude: currentUserLatitude,
    };
    this.props.postAmplitudeEvent(
      contactCardEvent,
      uid,
      userProperties,
      eventProperties,
      queryParams
    );
  }

  render() {
    const profilePhotoSource =
      this.state.profilePhotoUrl && this.state.profilePhotoUrl !== ''
        ? {
            isStatic: true,
            uri: this.state.profilePhotoUrl,
          }
        : {
            isStatic: true,
          };
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.onPressMapProfileCallout}>
          <Card flexDirection="row">
            <Avatar
              small
              rounded
              source={profilePhotoSource}
              activeOpacity={0.7}
            />
            <View>
              <Text>{`${this.state.firstName}`}</Text>
              <Text>{`${this.state.organization}`}</Text>
            </View>
          </Card>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  uid: state.user.user.uid,
  isProfileModalVisible: state.user.isProfileModalVisible,
  authenticatedUser: state.user.authenticatedUser,
  location: state.location,
});

function mapDispatchToProps(dispatch, ownProps) {
  return {
    setSelectedUser: selectedUser =>
      dispatch(userActions.setSelectedUser(selectedUser)),
    setIsProfileModalVisible: isProfileModalVisible =>
      dispatch(userActions.setIsProfileModalVisible(isProfileModalVisible)),
    postAmplitudeEvent: (
      eventType,
      userId,
      userProperties,
      eventProperties,
      queryParams
    ) =>
      dispatch(
        userActions.postAmplitudeEvent(
          eventType,
          userId,
          userProperties,
          eventProperties,
          queryParams
        )
      ),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MapProfileCallout);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
