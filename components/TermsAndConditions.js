import React from 'react';
import { connect } from 'react-redux';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { Card } from 'react-native-elements';
import Modal from 'react-native-modal';
import { CheckBox } from 'react-native-elements';
import Heading from '../components/text/Heading';
import * as userActions from '../actions/userActions';

class TermsAndConditions extends React.Component {
  constructor() {
    super();
    this.state = {
      isChecked: false,
    };
    this.agreeToTerms = this.agreeToTerms.bind(this);
  }

  agreeToTerms() {
    this.setState({ isChecked: true }, () => {
      this.props.agreeToTerms(this.props.currentUserUid);
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Modal isVisible={!this.props.hasAgreedToTerms}>
          <Card style={styles.card}>
            <ScrollView>
              <Heading
                marginTop={16}
                color="#050409"
                textAlign="center"
                element="h4"
                fontWeight="bold"
                paddingBottom={8}
              >
                Khemet End User License Agreement
              </Heading>
              <Text style={(styles.text, styles.heading)}>
                Khemet Copyright (c) 2018 Khemet LLC
              </Text>
              <Text style={(styles.text, styles.heading)}>1. LICENSE</Text>
              <Text style={styles.text}>
                By receiving, opening the file package, and/or using
                Khemet("Software") containing this software, you agree that this
                End User User License Agreement(EULA) is a legally binding and
                valid contract and agree to be bound by it. You agree to abide
                by the intellectual property laws and all of the terms and
                conditions of this Agreement.
              </Text>
              <Text style={styles.text}>
                Unless you have a different license agreement signed by Khemet
                LLC your use of Khemet indicates your acceptance of this license
                agreement and warranty.
              </Text>
              <Text style={styles.text}>
                Subject to the terms of this Agreement, Khemet LLC grants to you
                a limited, non-exclusive, non-transferable license, without
                right to sub-license, to use Khemet in accordance with this
                Agreement and any other written agreement with Khemet LLC.
                Khemet LLC does not transfer the title of Khemet to you; the
                license granted to you is not a sale. This agreement is a
                binding legal agreement between Khemet LLC and the purchasers or
                users of Khemet.
              </Text>
              <Text style={styles.text}>
                If you do not agree to be bound by this agreement, remove Khemet
                from your phone now and, if applicable, promptly return to
                Khemet LLC by mail any copies of Khemet and related
                documentation and packaging in your possession.
              </Text>
              <Text style={(styles.text, styles.heading)}>2. DISTRIBUTION</Text>
              <Text style={styles.text}>
                Khemet and the license herein granted shall not be copied,
                shared, distributed, re-sold, offered for re-sale, transferred
                or sub-licensed in whole or in part except that you may make one
                copy for archive purposes only. For information about
                redistribution of Khemet contact Khemet LLC.
              </Text>
              <Text style={(styles.text, styles.heading)}>
                3. USER AGREEMENT
              </Text>
              <Text style={(styles.text, styles.heading)}>3.1 Use</Text>
              <Text style={styles.text}>
                Your license to use Khemet is limited to the number of licenses
                purchased by you. You shall not allow others to use, copy or
                evaluate copies of Khemet.
              </Text>
              <Text style={(styles.text, styles.heading)}>
                3.2 Use Restrictions
              </Text>
              <Text style={styles.text}>
                You shall use Khemet in compliance with all applicable laws and
                not for any unlawful purpose. Without limiting the foregoing,
                use, display or distribution of Khemet together with material
                that is pornographic, racist, vulgar, obscene, defamatory,
                libelous, abusive, promoting hatred, discriminating or
                displaying prejudice based on religion, ethnic heritage, race,
                sexual orientation or age is strictly prohibited. There is no
                tolerance for objectionable content or abusive users on Khemet
              </Text>
              <Text style={styles.text}>
                Each licensed copy of Khemet may be used on one single phone
                location by one user. Use of Khemet means that you have loaded,
                installed, or run Khemet on a phone or similar device. If you
                install Khemet onto a multi-user platform, server or network,
                each and every individual user of Khemet must be licensed
                separately.
              </Text>
              <Text style={styles.text}>
                You may make one copy of Khemet for backup purposes, providing
                you only have one copy installed on one phone being used by one
                person. Other users may not use your copy of Khemet . The
                assignment, sublicense, networking, sale, or distribution of
                copies of Khemet are strictly forbidden without the prior
                written consent of Khemet LLC. It is a violation of this
                agreement to assign, sell, share, loan, rent, lease, borrow,
                network or transfer the use of Khemet. If any person other than
                yourself uses Khemet registered in your name, regardless of
                whether it is at the same time or different times, then this
                agreement is being violated and you are responsible for that
                violation!
              </Text>
              <Text style={(styles.text, styles.heading)}>
                3.3 Copyright Restriction
              </Text>
              <Text style={styles.text}>
                This Software contains copyrighted material, trade secrets and
                other proprietary material. You shall not, and shall not attempt
                to, modify, reverse engineer, disassemble or decompile Khemet.
                Nor can you create any derivative works or other works that are
                based upon or derived from Khemet in whole or in part.
              </Text>
              <Text style={styles.text}>
                Khemet LLC&#39;s name, logo and graphics file that represents
                Khemet shall not be used in any way to promote products
                developed with Khemet . Khemet LLC retains sole and exclusive
                ownership of all right, title and interest in and to Khemet and
                all Intellectual Property rights relating thereto.
              </Text>
              <Text style={styles.text}>
                Copyright law and international copyright treaty provisions
                protect all parts of Khemet, products and services. No program,
                code, part, image, audio sample, or text may be copied or used
                in any way by the user except as intended within the bounds of
                the single user program. All rights not expressly granted
                hereunder are reserved for Khemet LLC.
              </Text>
              <Text style={(styles.text, styles.heading)}>
                3.4 Limitation of Responsibility
              </Text>
              <Text style={styles.text}>
                You will indemnify, hold harmless, and defend Khemet LLC , its
                employees, agents and distributors against any and all claims,
                proceedings, demand and costs resulting from or in any way
                connected with your use of Khemet LLC&#39;s Software.
              </Text>
              <Text style={styles.text}>
                In no event (including, without limitation, in the event of
                negligence) will Khemet LLC , its employees, agents or
                distributors be liable for any consequential, incidental,
                indirect, special or punitive damages whatsoever (including,
                without limitation, damages for loss of profits, loss of use,
                business interruption, loss of information or data, or pecuniary
                loss), in connection with or arising out of or related to this
                Agreement, Khemet or the use or inability to use Khemet or the
                furnishing, performance or use of any other matters hereunder
                whether based upon contract, tort or any other theory including
                negligence.
              </Text>
              <Text style={styles.text}>
                Khemet LLC&#39;s entire liability, without exception, is limited
                to the customers&#39; reimbursement of the purchase price of the
                Software (maximum being the lesser of the amount paid by you and
                the suggested retail price as listed by Khemet LLC ) in exchange
                for the return of the product, all copies, registration papers
                and manuals, and all materials that constitute a transfer of
                license from the customer back to Khemet LLC.
              </Text>
              <Text style={(styles.text, styles.heading)}>3.5 Warranties</Text>
              <Text style={styles.text}>
                Except as expressly stated in writing, Khemet LLC makes no
                representation or warranties in respect of this Software and
                expressly excludes all other warranties, expressed or implied,
                oral or written, including, without limitation, any implied
                warranties of merchantable quality or fitness for a particular
                purpose.
              </Text>
              <Text style={(styles.text, styles.heading)}>
                3.6 Governing Law
              </Text>
              <Text style={styles.text}>
                This Agreement shall be governed by the law of the United States
                applicable therein. You hereby irrevocably attorn and submit to
                the non-exclusive jurisdiction of the courts of United States
                therefrom. If any provision shall be considered unlawful, void
                or otherwise unenforceable, then that provision shall be deemed
                severable from this License and not affect the validity and
                enforceability of any other provisions.
              </Text>
              <Text style={(styles.text, styles.heading)}>3.7 Termination</Text>
              <Text style={styles.text}>
                Any failure to comply with the terms and conditions of this
                Agreement will result in automatic and immediate termination of
                this license. Upon termination of this license granted herein
                for any reason, you agree to immediately cease use of Khemet and
                destroy all copies of Khemet supplied under this Agreement. The
                financial obligations incurred by you shall survive the
                expiration or termination of this license.
              </Text>
              <Text style={(styles.text, styles.heading)}>
                4. DISCLAIMER OF WARRANTY
              </Text>
              <Text style={styles.text}>
                THIS SOFTWARE AND THE ACCOMPANYING FILES ARE SOLD "AS IS" AND
                WITHOUT WARRANTIES AS TO PERFORMANCE OR MERCHANTABILITY OR ANY
                OTHER WARRANTIES WHETHER EXPRESSED OR IMPLIED. THIS DISCLAIMER
                CONCERNS ALL FILES GENERATED AND EDITED BY Khemet AS WELL.
              </Text>
              <Text style={(styles.text, styles.heading)}>
                5. CONSENT OF USE OF DATA
              </Text>
              <Text style={styles.text}>
                You agree that Khemet LLC may collect and use information
                gathered in any manner as part of the product support services
                provided to you, if any, related to Khemet. Khemet LLC may also
                use this information to provide notices to you which may be of
                use or interest to you.
              </Text>
              <CheckBox
                title="Agree"
                checked={this.state.isChecked}
                onPress={this.agreeToTerms}
              />
            </ScrollView>
          </Card>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  currentUserUid: state.user.user.uid,
  hasAgreedToTerms: state.user.hasAgreedToTerms,
});

function mapDispatchToProps(dispatch, ownProps) {
  return {
    agreeToTerms: currentUserUid =>
      dispatch(userActions.agreeToTerms(currentUserUid)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TermsAndConditions);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    paddingBottom: 8,
    fontSize: 16,
  },
  heading: {
    fontWeight: 'bold',
  },
});
