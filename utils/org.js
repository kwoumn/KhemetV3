export const shields = [
  require('../images/shields/org_1906.png'),
  require('../images/shields/org_1911_2.png'),
  require('../images/shields/org_1920.png'),
  require('../images/shields/org_1908.png'),
  require('../images/shields/org_1913.png'),
  require('../images/shields/org_1922.png'),
  require('../images/shields/org_1911.png'),
  require('../images/shields/org_1914.png'),
  require('../images/shields/org_1963.png'),
];
const organizationToShieldMap = {
  'Alpha Phi Alpha': 0,
  'Omega Psi Phi': 1,
  'Zeta Phi Beta': 2,
  'Alpha Kappa Alpha': 3,
  'Delta Sigma Theta': 4,
  'Sigma Gamma Rho': 5,
  'Kappa Alpha Psi': 6,
  'Phi Beta Sigma': 7,
  'Iota Phi Theta': 8,
};
export function getOrgIcon(orgName) {
  const shieldIndex = organizationToShieldMap[orgName];
  return shields[shieldIndex];
}
