import numberToWords from 'number-to-words';

export const serverBaseURI =
  'http://ec2-34-227-162-108.compute-1.amazonaws.com:3002';

export function getChapterUrl(year) {
  return `https://s3-ap-northeast-1.amazonaws.com/qmr-cloud-s3-dev/bank/${year}.json`;
}

export const VISIBILITY_TYPE = {
  ON: 'ON',
  OFF: 'OFF',
};

export const organizationToGreekLettersMap = {
  'Alpha Phi Alpha': 'ΑΦΑ',
  'Alpha Kappa Alpha': 'ΑΚΑ',
  'Kappa Alpha Psi': 'ΚΑΨ',
  'Omega Psi Phi': 'ΩΨΦ',
  'Delta Sigma Theta': 'ΔΣΘ',
  'Phi Beta Sigma': 'ΦΒΣ',
  'Zeta Phi Beta': 'ΖΦΒ',
  'Sigma Gamma Rho': 'ΣΓΡ',
  'Iota Phi Theta': 'ΙΦΘ',
};

export function formatLineNumber(lineNumber) {
  switch (lineNumber) {
    case '1':
      return 'Ace';
    case '2':
      return 'Deuce';
    case '3':
      return 'Tre';
    case '4':
      return 'Quad';
    default:
      return capitalize(numberToWords.toWords(parseInt(lineNumber)));
  }
}

function capitalize(str) {
  return str
    .charAt(0)
    .toUpperCase()
    .concat(str.slice(1).toLowerCase());
}
