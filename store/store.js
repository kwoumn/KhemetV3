import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/rootReducer';
import moment from 'moment';

const initialState = {
  registration: {
    chapter: '',
    school: '',
    lineNumber: '',
    crossingSeason: {
      season: 'Spring',
      year: moment()
        .year()
        .toString(),
    },
    isLoaded: true,
  },
  user: {
    isProfileModalVisible: false,
    selectedUser: {
      firstName: '',
      lastName: '',
      organization: '',
      chapter: '',
      lineNumber: '1',
      school: '',
      crossingSeason: '',
      crossingYear: '',
      occupation: '',
    },
    blocked: [],
    blockedBy: [],
    authenticatedUser: {},
  },
  location: {},
  messages: {
    userChannels: {},
  },
};

/* eslint-disable no-underscore-dangle */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/* eslint-enable */

const store = createStore(
  rootReducer,
  initialState,
  composeEnhancers(applyMiddleware(thunk))
);

export default store;
