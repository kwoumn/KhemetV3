import React from 'react';
import { Provider, connect } from 'react-redux';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import { FormattedWrapper } from 'react-native-globalize';
import LoginOrRegister from './scenes/LoginOrRegister';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Login from './scenes/Login';
import Register from './scenes/Register';
import ChooseOrg from './scenes/ChooseOrg';
import ChooseChapter from './scenes/ChooseChapter';
import ChooseSchool from './scenes/ChooseSchool';
import ChooseLineNumber from './scenes/ChooseLineNumber';
import ChooseCrossingSeason from './scenes/ChooseCrossingSeason';
import InputOccupation from './scenes/InputOccupation';
import GreekMap from './scenes/GreekMap';
import Chat from './scenes/Chat';
import MessageList from './scenes/MessageList';
import ProfileModal from './components/ProfileModal';
import TermsAndConditions from './components/TermsAndConditions';
import MapProfileCallout from './components/MapProfileCallout';
import Settings from './scenes/Settings';
import CurrentUserProfile from './scenes/CurrentUserProfile';
import store from './store/store';

import { Sentry } from 'react-native-sentry';

Sentry.config('https://c5dc8e6afbb44122bebdd19eb4544ea4@sentry.io/1279746').install();


const TabNavigationStack = createBottomTabNavigator(
  {
    greekMap: {
      screen: createStackNavigator({
        greekMap: {
          screen: GreekMap,
        }
      }),
      navigationOptions: {
        tabBarLabel: 'Greeks Near Me'
      }
    },
    currentUserProfile: {
      screen: createStackNavigator({
        currentUserProfile: {
          screen: CurrentUserProfile,
        }
      }),
      navigationOptions: {
        tabBarLabel: 'Profile'
      }
    }
  },
  {
    navigationOptions: ({navigation}) => ({
      tabBarIcon: () => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'greekMap') {
          iconName = 'map'
        } else if (routeName === 'currentUserProfile') {
          iconName = 'account'
        }
        return <Icon name={iconName} size={30} />
      },
      tabBarOptions: {
        activeTintColor: '#0084b4',
        inactiveTintColor: 'gray',
        labelStyle: {
          margin: 0,
          fontSize: 14,
        }
      },
    })
  }
)

TabNavigationStack.navigationOptions = {
  header: null,
};


const RootStack = createStackNavigator(
  {
    loginOrRegister: LoginOrRegister,
    register: Register,
    login: Login,
    chooseChapter: ChooseChapter,
    chooseLineNumber: ChooseLineNumber,
    chooseSchool: ChooseSchool,
    chooseCrossingSeason: ChooseCrossingSeason,
    chooseOrg: ChooseOrg,
    inputOccupation: InputOccupation,
    greekMap: TabNavigationStack,
    chat: Chat,
    mapProfileCallout: MapProfileCallout,
    termsAndConditons: TermsAndConditions,
    messageList: MessageList,
    settings: Settings,
    currentUserProfile: CurrentUserProfile,
  },
  {
    initialRouteName: 'loginOrRegister',
  }
)

export default class App extends React.Component {
  render() {
    return (
      <FormattedWrapper locale="en" currency="USD">
        <Provider store={store}>
          <RootStack />
        </Provider>
      </FormattedWrapper>
    );
  }
}
