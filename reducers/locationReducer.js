import * as types from '../actions/actionTypes';

const location = (state = {}, action) => {
  switch (action.type) {
    case types.SET_VISIBILITY:
      return {
        ...state,
        visibilityType: action.data.visibilityType,
      };
    case types.SET_LOCATION:
      return {
        ...state,
        latitude: action.data.latitude,
        longitude: action.data.longitude,
      };
    case types.SET_LOCATIONS_TO_DISPLAY:
      return {
        ...state,
        locationsToDisplay: action.data.locationsToDisplay,
      };
    default:
      return state;
  }
};

export default location;
