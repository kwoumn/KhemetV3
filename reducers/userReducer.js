import * as types from '../actions/actionTypes';

const user = (state = {}, action) => {
  switch (action.type) {
    case types.SET_USER:
      return {
        ...state,
        user: action.data.user,
      };
    case types.SET_SELECTED_USER:
      return {
        ...state,
        selectedUser: action.data.selectedUser,
      };
    case types.SET_IS_PROFILE_MODAL_VISIBLE:
      return {
        ...state,
        isProfileModalVisible: action.data.isProfileModalVisible,
      };
    case types.SET_AUTHENTICATED_USER_ATTRIBUTES:
      return {
        ...state,
        authenticatedUser: action.data.userAttributes,
      };
    case types.SET_BLOCKED_USERS:
      return {
        ...state,
        blocked: action.data.blockedUsers,
      };
    case types.SET_BLOCKED_BY_USERS:
      return {
        ...state,
        blockedBy: action.data.blockedByUsers,
      };
    case types.SET_HAS_AGREED_TO_TERMS:
      return {
        ...state,
        hasAgreedToTerms: action.data.hasAgreedToTerms,
      };
    default:
      return state;
  }
};

export default user;
