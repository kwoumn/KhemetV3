import * as types from '../actions/actionTypes';

const registration = (state = {}, action) => {
  switch (action.type) {
    case types.SET_ORGANIZATION:
      return {
        ...state,
        organization: action.data.organization,
      };
    case types.SET_CHAPTER_LIST:
      return {
        ...state,
        chapterList: action.data.chapterList,
      };
    case types.SET_CHAPTER:
      return {
        ...state,
        chapter: action.data.chapter,
      };
    case types.SET_SCHOOL:
      return {
        ...state,
        school: action.data.school,
      };
    case types.SET_LINE_NUMBER:
      return {
        ...state,
        lineNumber: action.data.lineNumber,
      };
    case types.SET_CROSSING_SEASON:
      return {
        ...state,
        crossingSeason: {
          season: action.data.season,
          year: action.data.year,
        },
      };
    case types.SET_OCCUPATION:
      return {
        ...state,
        occupation: action.data.occupation,
      };
    case types.SET_LOADING:
      return {
        ...state,
        isLoaded: action.data.isLoaded,
      };
    default:
      return state;
  }
};

export default registration;
