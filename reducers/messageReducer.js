import * as types from '../actions/actionTypes';
import moment from 'moment';
import { GiftedChat } from 'react-native-gifted-chat';

const messages = (state = {}, action) => {
  switch (action.type) {
    case types.SET_USER_CHANNELS:
      return {
        ...state,
        userChannels: convertToUserChannelsObject(action.data.userChannels),
      };
    case types.UPDATE_USER_CHANNELS:
      return {
        ...state,
        userChannels: Object.assign(
          {},
          updateUserChannels(
            state.userChannels,
            action.data.messagePayload,
            action.data.messageBody,
            action.data.channelSid,
            action.data.dateUpdated
          )
        ),
      };
    case types.SET_NEW_MESSAGE:
      const prevMessages =
        state.userChannels[action.data.channelSid] &&
        state.userChannels[action.data.channelSid].messages
          ? state.userChannels[action.data.channelSid].messages
          : [];
      return {
        ...state,
        userChannels: {
          ...state.userChannels,
          [action.data.channelSid]: {
            ...state.userChannels[action.data.channelSid],
            messages: GiftedChat.append(
              prevMessages,
              action.data.messagePayload
            ),
          },
        },
      };
    case types.SET_NEW_MESSAGE_CHANNEL:
      return {
        ...state,
        userChannels: {
          ...state.userChannels,
          [action.data.message.channel.sid]: newMessageChannel(
            action.data.message,
            action.data.members,
            action.data.recipientUid
          ),
        },
      };
    case types.SET_PUSH_PLAYER_ID:
      return {
        ...state,
        pushPlayerId: action.data.pushPlayerId,
      };
    default:
      return state;
  }
};

function newMessageChannel(message, members, recipientUid) {
  return {
    members,
    recipientUid,
    lastMessage: message.body,
    messages: [
      {
        _id: message.sid,
        text: message.body,
        createdAt: message.timestamp,
        user: {
          _id: message.author,
        },
      },
    ],
    channelSid: message.channel.sid,
    dateUpdated: moment(message.dateUpdated).format(),
    authorUid: message.author,
  };
}

function convertToUserChannelsObject(userChannels) {
  let userChannelsObj = {};
  userChannels.forEach(userChannel => {
    userChannelsObj[userChannel.channelSid] = userChannel;
  });
  return userChannelsObj;
}

function updateUserChannels(
  userChannels,
  messagePayload,
  messageBody,
  channelSid,
  dateUpdated
) {
  userChannels[channelSid] = {
    ...userChannels[channelSid],
    messages: [messagePayload, ...userChannels[channelSid].messages],
    lastMessage: messageBody,
    dateUpdated,
  };
  return userChannels;
}

export default messages;
