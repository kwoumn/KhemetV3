import { combineReducers } from 'redux';
import registration from './registrationReducer';
import user from './userReducer';
import location from './locationReducer';
import messages from './messageReducer';

const rootReducer = combineReducers({
  registration,
  user,
  location,
  messages,
});

export default rootReducer;
