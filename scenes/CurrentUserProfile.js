import React from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  organizationToGreekLettersMap,
  formatLineNumber,
} from '../api/constants';

class CurrentUserProfile extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Profile',
      headerRight: (
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            style={{ paddingRight: 15 }}
            onPress={() => navigation.navigate('messageList')}
          >
            <Icon name="inbox" size={30} />
          </TouchableOpacity>
          <TouchableOpacity
            style={{ paddingRight: 15 }}
            onPress={() => navigation.navigate('settings')}
          >
            <Icon name="settings" size={30} />
          </TouchableOpacity>
        </View>
      ),
    };
  };
  render() {
    const { authenticatedUser } = this.props;
    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.headerBackgroundImage}
          blurRadius={10}
          source={require('../images/Nimvelo.jpg')}
        >
          <View style={styles.imageContainer}>
            <Image
              style={styles.userImage}
              source={{
                uri: authenticatedUser.profilePhotoUrl,
              }}
            />
          </View>
          <Text style={styles.userNameText}>{authenticatedUser.firstName}</Text>
        </ImageBackground>
        <ScrollView>
          <View style={styles.itemView}>
            <Text style={styles.sectionHeader}>Organization</Text>
            <Text style={styles.textContent}>
              {organizationToGreekLettersMap[authenticatedUser.organization]}
            </Text>
          </View>
          <View style={styles.itemView}>
            <Text style={styles.sectionHeader}>Chapter</Text>
            <Text style={styles.textContent}>{authenticatedUser.chapter}</Text>
          </View>
          <View style={styles.itemView}>
            <Text style={styles.sectionHeader}>School</Text>
            <Text style={styles.textContent}>{authenticatedUser.school}</Text>
          </View>
          <View style={styles.itemView}>
            <Text style={styles.sectionHeader}>Crossing Season</Text>
            <Text style={styles.textContent}>
              {authenticatedUser.crossingSeason}{' '}
              {authenticatedUser.crossingYear}
            </Text>
          </View>
          <View style={styles.itemView}>
            <Text style={styles.sectionHeader}>Line Number</Text>
            <Text style={styles.textContent}>
              {formatLineNumber(authenticatedUser.lineNumber)} Club
            </Text>
          </View>
          <View style={styles.itemView}>
            <Text style={styles.sectionHeader}>Occupation</Text>
            <Text style={styles.textContent}>
              {authenticatedUser.occupation}
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  userImage: {
    borderColor: '#000',
    borderRadius: 85,
    borderWidth: 3,
    height: 170,
    marginBottom: 15,
    width: 170,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  userNameText: {
    color: '#FFFFFF',
    fontSize: 22,
    fontWeight: 'bold',
    paddingBottom: 8,
    textAlign: 'center',
  },
  headerBackgroundImage: {
    paddingBottom: 20,
    paddingTop: 35,
  },
  itemView: {
    margin: 12,
  },
  sectionHeader: {
    color: '#424242',
    fontSize: 23,
    fontWeight: '700',
  },
  textContent: {
    color: '#9E9E9E',
    fontSize: 17,
    marginTop: 4,
    marginBottom: 4,
  },
});

const mapStateToProps = state => ({
  authenticatedUser: state.user.authenticatedUser,
});

export default connect(mapStateToProps)(CurrentUserProfile);
