import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, StyleSheet, View } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';
import { ListItem } from 'react-native-elements';
import * as actions from '../actions/registerActions';

class ChooseChapter extends React.Component {
  constructor() {
    super();
    this.state = {
      filtering: true,
    };
    this.filterChapters = this.filterChapters.bind(this);
    this.onChapterSelect = this.onChapterSelect.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Chapter',
    };
  };

  filterChapters(query) {
    const chapters = this.props.registration.chapterList;
    if (!chapters) {
      return [];
    }
    if (!query || query === '') {
      return chapters;
    }
    const queryRegex = new RegExp(`${query.trim()}`, 'i');
    const results = chapters.filter(chapter => chapter.search(queryRegex) >= 0);
    return results;
  }

  onChapterSelect(chapter) {
    this.props.setChapter(chapter);
    this.setState({ filtering: false });
    this.props.navigation.pop();
  }

  render() {
    const chapter = this.props.registration.chapter;
    const filteredChapters = this.filterChapters(chapter);
    return (
      <View style={styles.container}>
        <TextField
          label="Input Chapter"
          autoFocus={true}
          value={chapter}
          onChangeText={chapter => this.props.setChapter(chapter)}
        />
        {filteredChapters &&
          filteredChapters.length > 0 && (
            <FlatList
              data={filteredChapters}
              renderItem={({ item, index }) => (
                <ListItem
                  title={item}
                  key={index}
                  onPress={() => this.onChapterSelect(item)}
                  hideChevron={true}
                />
              )}
              keyExtractor={(item, index) => index.toString()}
            />
          )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  registration: state.registration,
});

function mapDispatchToProps(dispatch) {
  return {
    setChapter: chapter => dispatch(actions.setChapter(chapter)),
  };
}

ChooseChapter.propTypes = {
  registration: PropTypes.shape({
    organization: PropTypes.shape({
      name: PropTypes.string.isRequired,
      yearFounded: PropTypes.string.isRequired,
    }),
    chapterList: PropTypes.arrayOf(PropTypes.string),
    chapter: PropTypes.string.isRequired,
  }).isRequired,
  setChapter: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ChooseChapter);
