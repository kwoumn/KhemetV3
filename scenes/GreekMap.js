import React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import Mapbox from '@mapbox/react-native-mapbox-gl';
import MapboxGL from '@mapbox/react-native-mapbox-gl';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import OneSignal from 'react-native-onesignal';
import ActionSheet from 'react-native-actionsheet';
import { MAPBOX_ACCESS_TOKEN } from '../api/firebaseConstants';
import { VISIBILITY_TYPE } from '../api/constants';
import * as locationActions from '../actions/locationActions';
import * as userActions from '../actions/userActions';
import * as chatActions from '../actions/chatActions';
import MapProfileCallout from '../components/MapProfileCallout';
import ProfileModal from '../components/ProfileModal';
import TermsAndConditions from '../components/TermsAndConditions';

Mapbox.setAccessToken(MAPBOX_ACCESS_TOKEN);

class GreekMaps extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Greeks Near Me',
      headerRight: (
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            style={{ paddingRight: 15 }}
            onPress={() => navigation.navigate('messageList')}
          >
            <Icon name="inbox" size={30} />
          </TouchableOpacity>
          <TouchableOpacity
            style={{ paddingRight: 15 }}
            onPress={() => navigation.navigate('settings')}
          >
            <Icon name="settings" size={30} />
          </TouchableOpacity>
        </View>
      ),
    };
  };
  constructor() {
    super();
    this.onIds = this.onIds.bind(this);
    this.state = {
      isLoaded: false,
    };
    this.actionSheetOptions = ['Report', 'Cancel'];
    this.renderAnnotations = this.renderAnnotations.bind(this);
    this.showActionSheet = this.showActionSheet.bind(this);
    this.onPressActionSheet = this.onPressActionSheet.bind(this);
  }

  componentDidMount() {
    // Initialize OneSignal for push notifications
    OneSignal.init('a246b292-475f-4eb0-b8b1-00f4ed86b376', {
      kOSSettingsKeyAutoPrompt: true,
    });
    OneSignal.configure();
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.inFocusDisplaying(2);

    this.props.hasAgreedToTerms(this.props.uid).then(() => {
      this.setState({ isLoaded: true });
      this.props.getCurrentUserVisibility(this.props.uid);
      this.props
        .getAuthenticatedUserAttributes(this.props.uid)
        .then(userProperties => {
          this.props.postAmplitudeEvent('greek_map', this.props.uid, {
            organization: userProperties.organization,
            chapter: userProperties.chapter,
            school: userProperties.school,
            crossingSeason: userProperties.crossingSeason,
            crossingYear: userProperties.crossingYear,
            lineNumber: userProperties.lineNumber,
          });
        });
      this.props.getChannelsForUser(this.props.uid);
      this.watchId = navigator.geolocation.watchPosition(
        position => {
          this.props.storeLocation(
            position.coords.latitude,
            position.coords.longitude
          );
          this.props.getLocationsToDisplay();
        },
        error => this.setState({ error: error.message }),
        {
          enableHighAccuracy: true,
          timeout: 20000,
          maximumAge: 600000, //one minute
          distanceFilter: 10,
        }
      );
      this.props.onLocationAdded();
      this.props.onLocationRemoved();
      this.props.getBlockedUsers(this.props.uid);
      this.props.getBlockedByUsers(this.props.uid);
    });
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('ids', this.onIds);
    navigator.geolocation.clearWatch(this.watchId);
  }

  onIds(device) {
    this.props.setPushId(this.props.uid, device.userId);
  }

  showActionSheet() {
    this.ActionSheet.show();
  }

  onPressActionSheet(index) {
    switch (index) {
      case 0:
        this.props.reportProfile(this.props.uid, this.props.selectedUser.uid);
        break;
    }
  }

  render() {
    const { isLoaded } = this.state;
    const { terms, visibilityType } = this.props;
    const isVisible = visibilityType === 'ON';
    return isLoaded ? (
      terms ? (
        <View style={styles.container}>
          <MapboxGL.MapView
            styleURL={Mapbox.StyleURL.Street}
            zoomLevel={13}
            maxZoomLevel={13}
            userTrackingMode={MapboxGL.UserTrackingModes.Follow}
            showUserLocation={true}
            style={styles.container}
            logoEnabled={false}
            pitchEnabled={false}
            rotateEnabled={false}
          >
            <Button
              title={!isVisible ? 'Check-In' : 'Checked-In'}
              style={styles.checkIn}
              backgroundColor={!isVisible ? 'white' : 'green'}
              color={!isVisible ? 'black' : 'white'}
              leftIcon={{
                name: 'map-marker',
                color: 'red',
                type: 'material-community',
                size: 20,
              }}
              rounded
              raised
              onPress={() =>
                this.props.updateVisibility(this.props.uid, !isVisible)
              }
            />
            {this.renderAnnotations()}
            <ProfileModal
              navigation={this.props.navigation}
              showActionSheet={this.showActionSheet}
            />
          </MapboxGL.MapView>
          <ActionSheet
            ref={o => (this.ActionSheet = o)}
            options={this.actionSheetOptions}
            destructiveButtonIndex={0}
            cancelButtonIndex={1}
            onPress={index => this.onPressActionSheet(index)}
          />
        </View>
      ) : (
        <TermsAndConditions />
      )
    ) : (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }

  renderAnnotations() {
    return (
      this.props.locations &&
      this.props.locations.map((location, index) => {
        return (
          location.visibilty === VISIBILITY_TYPE.ON && (
            <MapboxGL.PointAnnotation
              key={index}
              id={location.uid}
              coordinate={[
                parseFloat(location.longitude),
                parseFloat(location.latitude),
              ]}
              title="Current Location"
            >
              <MapboxGL.Callout title="Hello">
                <MapProfileCallout
                  getUserInfoFromUid={() =>
                    this.props.getUserInfoFromUid(location.uid)
                  }
                  selectedUserUid={location.uid}
                  navigation={this.props.navigation}
                />
              </MapboxGL.Callout>
            </MapboxGL.PointAnnotation>
          )
        );
      })
    );
  }
}

const mapStateToProps = state => ({
  uid: state.user.user.uid,
  terms: state.user.hasAgreedToTerms,
  locations: state.location.locationsToDisplay,
  visibilityType: state.location.visibilityType,
  selectedUser: state.user.selectedUser,
});

function mapDispatchToProps(dispatch, ownProps) {
  return {
    storeLocation: (latitude, longitude) =>
      dispatch(locationActions.storeLocation(latitude, longitude)),
    getLocationsToDisplay: () =>
      dispatch(locationActions.getLocationsToDisplay()),
    onLocationAdded: () => dispatch(locationActions.onLocationAdded()),
    onLocationRemoved: () => dispatch(locationActions.onLocationRemoved()),
    getUserInfoFromUid: uid =>
      dispatch(locationActions.getUserInfoFromUid(uid)),
    getCurrentUserVisibility: uid =>
      dispatch(locationActions.getCurrentUserVisibility(uid)),
    updateVisibility: (uid, shouldBeVisible) =>
      dispatch(locationActions.updateVisibility(uid, shouldBeVisible)),
    getAuthenticatedUserAttributes: uid =>
      dispatch(userActions.getAuthenticatedUserAttributes(uid)),
    getChannelsForUser: uid => dispatch(chatActions.getChannelsForUser(uid)),
    setPushId: (uid, playerId) =>
      dispatch(chatActions.setPushId(uid, playerId)),
    postAmplitudeEvent: (
      eventType,
      userId,
      userProperties,
      eventProperties,
      queryParams
    ) =>
      dispatch(
        userActions.postAmplitudeEvent(
          eventType,
          userId,
          userProperties,
          eventProperties,
          queryParams
        )
      ),
    getBlockedUsers: currentUserUid =>
      dispatch(userActions.getBlockedUsers(currentUserUid)),
    getBlockedByUsers: currentUserUid =>
      dispatch(userActions.getBlockedByUsers(currentUserUid)),
    hasAgreedToTerms: currentUserUid =>
      dispatch(userActions.hasAgreedToTerms(currentUserUid)),
    reportProfile: (currentUserUid, offendingUserUid) =>
      dispatch(userActions.reportProfile(currentUserUid, offendingUserUid)),
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  checkIn: {
    alignSelf: 'flex-end',
    marginTop: 30,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(GreekMaps);
