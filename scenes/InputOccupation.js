import React from 'react';
import PropTypes from 'prop-types';
import {
  Keyboard,
  View,
  SafeAreaView,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import { connect } from 'react-redux';
import BottomButton from '../components/BottomButton';
import { TextField } from 'react-native-material-textfield';
import * as actions from '../actions/registerActions';

class InputOccupation extends React.Component {
  constructor() {
    super();
    this.onSelectDone = this.onSelectDone.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Occupation',
    };
  };

  onSelectDone() {
    this.props.navigation.pop();
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.container}>
            <TextField
              label="Input Occupation"
              title="Ex. Student, Engineer, Consultant"
              value={this.props.registration.occupation}
              onChangeText={occupation => this.props.setOccupation(occupation)}
            />
            <BottomButton onPress={this.onSelectDone}>Done</BottomButton>
          </View>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  registration: state.registration,
});

function mapDispatchToProps(dispatch) {
  return {
    setOccupation: occupation => dispatch(actions.setOccupation(occupation)),
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
});

InputOccupation.propTypes = {
  registration: PropTypes.shape({
    occupation: PropTypes.string,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(InputOccupation);
