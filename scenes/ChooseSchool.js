import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { ListItem } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import * as actions from '../actions/registerActions';

const schools = require('../data/universities.json');

class ChooseSchool extends React.Component {
  constructor() {
    super();
    this.state = {
      filtering: true,
    };
    this.filterSchoolName = this.filterSchoolName.bind(this);
    this.onSchoolSelect = this.onSchoolSelect.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'School',
    };
  };

  filterSchoolName(query) {
    if (!schools) {
      return [];
    }
    if (!query || query === '') {
      return schools;
    }
    const queryRegex = new RegExp(`${query.trim()}`, 'i');
    const results = schools.filter(school => school.search(queryRegex) >= 0);
    return results;
  }

  onSchoolSelect(school) {
    this.props.setSchool(school);
    this.setState({ filtering: false });
    this.props.navigation.pop();
  }

  render() {
    const school = this.props.registration.school;
    const filteredSchools = this.filterSchoolName(school);
    return (
      <View style={styles.container}>
        <TextField
          autoFocus={true}
          label="Input School"
          value={school}
          onChangeText={school => this.props.setSchool(school)}
        />
        {filteredSchools &&
          filteredSchools.length > 0 && (
            <FlatList
              data={filteredSchools}
              renderItem={({ item, index }) => (
                <ListItem
                  title={item}
                  key={index}
                  onPress={() => this.onSchoolSelect(item)}
                />
              )}
              keyExtractor={(item, index) => index.toString()}
            />
          )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
});

const mapStateToProps = state => ({
  registration: state.registration,
});

function mapDispatchToProps(dispatch) {
  return {
    setSchool: school => dispatch(actions.setSchool(school)),
  };
}

ChooseSchool.propTypes = {
  registration: PropTypes.shape({
    organization: PropTypes.shape({
      name: PropTypes.string.isRequired,
      yearFounded: PropTypes.string.isRequired,
    }),
    chapterList: PropTypes.arrayOf(PropTypes.string),
    chapter: PropTypes.string.isRequired,
    school: PropTypes.string.isRequired,
  }).isRequired,
  setSchool: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChooseSchool);
