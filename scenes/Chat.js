import React from 'react';
import {
  ActivityIndicator,
  Keyboard,
  View,
  SafeAreaView,
  StyleSheet,
  Linking,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { GiftedChat } from 'react-native-gifted-chat';
import OneSignal from 'react-native-onesignal';
import ActionSheet from 'react-native-actionsheet';
import * as chatActions from '../actions/chatActions';
import * as userActions from '../actions/userActions';

class Chat extends React.Component {
  constructor() {
    super();
    this.actionSheetOptions = ['Report', 'Cancel'];
    this.state = {
      messages: [],
      channel: {},
      chatClient: {},
      isLoaded: false,
    };

    this.parsePatterns = this.parsePatterns.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.createTwoPersonChannel = this.createTwoPersonChannel.bind(this);
    this.doesExistingConversationExist = this.doesExistingConversationExist.bind(
      this
    );
    this.getMessages = this.getMessages.bind(this);
    this.messagesInChannel = this.messagesInChannel.bind(this);
    this.messageSentEvent = this.messageSentEvent.bind(this);
    this.showActionSheet = this.showActionSheet.bind(this);
    this.onPressActionSheet = this.onPressActionSheet.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    const showActionSheet = navigation.getParam('showActionSheet', () => {});
    return {
      headerTitle: navigation.getParam('recipientName', ''),
      headerRight: (
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            style={{ paddingRight: 15 }}
            onPress={showActionSheet}
          >
            <Icon name="dots-horizontal" size={30} />
          </TouchableOpacity>
        </View>
      ),
    };
  };

  componentDidMount() {
    if (
      this.props.navigation.state.params &&
      this.props.navigation.state.params.channelSid
    ) {
      //Channel sid if we are loading Chat screen from the message list
      // this.setState({ isLoaded: true });
    }
    this.props.navigation.setParams({ showActionSheet: this.showActionSheet });
    this.doesExistingConversationExist().then(channel => {
      if (channel) {
        this.setState({ channel, isLoaded: true });
      } else {
        this.createTwoPersonChannel(this.state.chatClient);
      }
    });
  }

  getMessages(channel) {
    return channel.getMessages().then(messagePaginator => {
      return messagePaginator.items.reverse().map(message => {
        return {
          _id: message.sid,
          text: message.body,
          createdAt: message.timestamp,
          user: {
            _id: message.author,
          },
        };
      });
    });
  }

  doesExistingConversationExist() {
    return this.props
      .createChatClient(this.props.authorUid)
      .then(chatClient => {
        this.setState({ chatClient });
        return chatClient;
      })
      .then(chatClient => {
        return chatClient.getUserChannelDescriptors();
      })
      .then(paginator => {
        return paginator.items.map(channelDescriptor => {
          return channelDescriptor.getChannel();
        });
      })
      .then(channelsPromises => {
        return Promise.all(channelsPromises).then(channels => {
          return channels;
        });
      })
      .then(channels => {
        return channels.filter(channel => {
          return channel.getMembersCount().then(memberCount => {
            return memberCount === 2;
          });
        });
      })
      .then(twoWayChannelsPromises => {
        return Promise.all(twoWayChannelsPromises).then(twoWayChannels => {
          return twoWayChannels;
        });
      })
      .then(twoWayChannels => {
        return twoWayChannels.map(twoWayChannel => {
          return twoWayChannel.getMembers().then(members => {
            return {
              channel: twoWayChannel,
              members,
            };
          });
        });
      })
      .then(twoWayChannelsMembersPromises => {
        return Promise.all(twoWayChannelsMembersPromises).then(
          twoWayChannelMembers => {
            return twoWayChannelMembers;
          }
        );
      })
      .then(twoWayChannelsMembers => {
        return twoWayChannelsMembers.find(twoWayChannelMembers => {
          return (
            twoWayChannelMembers.members.find(
              member => member.identity === this.props.recipientUid
            ) !== undefined
          );
        });
      })
      .then(channel => {
        if (channel) {
          return channel.channel;
        } else {
          return false;
        }
      });
  }

  createTwoPersonChannel(chatClient) {
    chatClient
      .createChannel({
        isPrivate: true,
      })
      .then(channel => {
        channel
          .join()
          .then(channel => {
            this.setState({ channel, isLoaded: true });
            channel.add(this.props.recipientUid).catch(error => {
              console.error(error);
            });
          })
          .catch(error => console.error(error));
      });
  }

  parsePatterns(linkStyle) {
    return [
      {
        pattern: /#(\w+)/,
        style: { ...linkStyle, color: 'orange' },
        onPress: () => Linking.openURL('http://gifted.chat'),
      },
    ];
  }

  sendMessage(messages = []) {
    this.state.channel
      .sendMessage(messages[0].text)
      .then(messageId => {
        Keyboard.dismiss();
        this.messageSentEvent();
      })
      .catch(err => {
        console.error(err);
      });
  }

  messageSentEvent() {
    const { authenticatedUser, authorUid } = this.props;
    const messageSentEvent = 'message_sent';
    const userProperties = {
      organization: authenticatedUser.organization,
      chapter: authenticatedUser.chapter,
      school: authenticatedUser.school,
      crossingSeason: authenticatedUser.crossingSeason,
      crossingYear: authenticatedUser.crossingYear,
      lineNumber: authenticatedUser.lineNumber,
    };
    this.props.postAmplitudeEvent(messageSentEvent, authorUid, userProperties);
  }

  messagesInChannel() {
    const { userChannels } = this.props;
    const { channel } = this.state;
    const channelSid =
      this.props.navigation.state.params &&
      this.props.navigation.state.params.channelSid;
    let messagesInChannel;
    if (channelSid) {
      messagesInChannel = userChannels[channelSid].messages;
    } else if (userChannels[channel.sid]) {
      messagesInChannel = userChannels[channel.sid].messages;
    } else {
      messagesInChannel = [];
    }
    return messagesInChannel;
  }

  showActionSheet() {
    this.ActionSheet.show();
  }

  onPressActionSheet(index) {
    const channelSid =
      (this.props.navigation.state.params &&
        this.props.navigation.state.params.channelSid) ||
      this.state.channel.sid;
    switch (index) {
      case 0:
        if (channelSid) {
          this.props.reportConversation(this.props.authorUid, channelSid);
        }
        break;
    }
  }

  render() {
    const { authorUid } = this.props;
    const messagesInChannel = this.messagesInChannel();
    return this.state.isLoaded ? (
      <SafeAreaView style={styles.container}>
        <GiftedChat
          messages={messagesInChannel}
          onSend={messages => this.sendMessage(messages)}
          user={{
            _id: authorUid,
          }}
          parsePatterns={this.parsePatterns}
        />
        <ActionSheet
          ref={o => (this.ActionSheet = o)}
          options={this.actionSheetOptions}
          destructiveButtonIndex={0}
          cancelButtonIndex={1}
          onPress={index => this.onPressActionSheet(index)}
        />
      </SafeAreaView>
    ) : (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const mapStateToProps = (state, props) => ({
  authorUid: state.user.user.uid || props.navigation.state.params.authorUid,
  recipientUid:
    state.user.selectedUser.uid || props.navigation.state.params.recipientUid,
  userChannels: state.messages.userChannels,
  authenticatedUser: state.user.authenticatedUser,
});

function mapDispatchToProps(dispatch) {
  return {
    createChatClient: authorUid =>
      dispatch(chatActions.createChatClient(authorUid)),
    setNewMessage: (channelSid, messagePayload) =>
      dispatch(chatActions.setNewMessage(channelSid, messagePayload)),
    postAmplitudeEvent: (
      eventType,
      userId,
      userProperties,
      eventProperties,
      queryParams
    ) =>
      dispatch(
        userActions.postAmplitudeEvent(
          eventType,
          userId,
          userProperties,
          eventProperties,
          queryParams
        )
      ),
    reportConversation: (reportingUserUid, channelSid) =>
      dispatch(userActions.reportConversation(reportingUserUid, channelSid)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
