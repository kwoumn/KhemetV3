import React from 'react';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../actions/registerActions';
import { shields } from '../utils/org';

const orgToYearChapterMap = {
  'Alpha Phi Alpha': '1906',
  'Alpha Kappa Alpha': '1908',
  'Kappa Alpha Psi': '1911',
  'Omega Psi Phi': '1911_2',
  'Delta Sigma Theta': '1913',
  'Phi Beta Sigma': '1914',
  'Zeta Phi Beta': '1920',
  'Sigma Gamma Rho': '1922',
  'Iota Phi Theta': '1963',
};

class RowItem extends React.Component {
  constructor() {
    super();
    this.handleSelectOrganization = this.handleSelectOrganization.bind(this);
  }

  handleSelectOrganization(index) {
    const organization = index => {
      switch (index) {
        case 0:
          return {
            name: 'Alpha Phi Alpha',
            yearFounded: '1906',
          };
        case 1:
          return {
            name: 'Omega Psi Phi',
            yearFounded: '1911',
          };
        case 2:
          return {
            name: 'Zeta Phi Beta',
            yearFounded: '1920',
          };
        case 3:
          return {
            name: 'Alpha Kappa Alpha',
            yearFounded: '1908',
          };
        case 4:
          return {
            name: 'Delta Sigma Theta',
            yearFounded: '1913',
          };
        case 5:
          return {
            name: 'Sigma Gamma Rho',
            yearFounded: '1922',
          };
        case 6:
          return {
            name: 'Kappa Alpha Psi',
            yearFounded: '1911',
          };
        case 7:
          return {
            name: 'Phi Beta Sigma',
            yearFounded: '1914',
          };
        case 8:
          return {
            name: 'Iota Phi Theta',
            yearFounded: '1963',
          };
      }
    };
    this.props.setOrganization(organization(index));
    const orgName = organization(index).name;
    const mappedYear = orgToYearChapterMap[orgName];
    this.props.getChapterList(mappedYear);
    this.props.navigation.pop();
  }

  render() {
    const { index } = this.props;
    return (
      <View style={{ flexDirection: 'row', flex: 1 }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <TouchableOpacity
            onPress={() => this.handleSelectOrganization(index)}
            key={1}
          >
            <Image
              source={shields[index]}
              style={styles.image}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export class ChooseOrg extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Organization',
    };
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', flex: 1 }}>
          <View style={{ flexDirection: 'column', flex: 1 }}>
            <RowItem
              index={0}
              setOrganization={this.props.setOrganization}
              getChapterList={this.props.getChapterList}
              navigation={this.props.navigation}
            />
            <RowItem
              index={1}
              setOrganization={this.props.setOrganization}
              getChapterList={this.props.getChapterList}
              navigation={this.props.navigation}
            />
            <RowItem
              index={2}
              setOrganization={this.props.setOrganization}
              getChapterList={this.props.getChapterList}
              navigation={this.props.navigation}
            />
          </View>
          <View style={{ flexDirection: 'column', flex: 1 }}>
            <RowItem
              index={3}
              setOrganization={this.props.setOrganization}
              getChapterList={this.props.getChapterList}
              navigation={this.props.navigation}
            />
            <RowItem
              index={4}
              setOrganization={this.props.setOrganization}
              getChapterList={this.props.getChapterList}
              navigation={this.props.navigation}
            />
            <RowItem
              index={5}
              setOrganization={this.props.setOrganization}
              getChapterList={this.props.getChapterList}
              navigation={this.props.navigation}
            />
          </View>
          <View style={{ flexDirection: 'column', flex: 1 }}>
            <RowItem
              index={6}
              setOrganization={this.props.setOrganization}
              getChapterList={this.props.getChapterList}
              navigation={this.props.navigation}
            />
            <RowItem
              index={7}
              setOrganization={this.props.setOrganization}
              getChapterList={this.props.getChapterList}
              navigation={this.props.navigation}
            />
            <RowItem
              index={8}
              setOrganization={this.props.setOrganization}
              getChapterList={this.props.getChapterList}
              navigation={this.props.navigation}
            />
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  registration: state.registration,
});

function mapDispatchToProps(dispatch) {
  return {
    setOrganization: organization =>
      dispatch(actions.setOrganization(organization)),
    getChapterList: yearFounded =>
      dispatch(actions.getChapterList(yearFounded)),
  };
}

ChooseOrg.propTypes = {
  registration: PropTypes.shape({
    organization: PropTypes.shape({
      name: PropTypes.string.isRequired,
      yearFounded: PropTypes.string.isRequired,
    }),
    chapterList: PropTypes.arrayOf(PropTypes.string),
    chapter: PropTypes.string.isRequired,
  }).isRequired,
  setOrganization: PropTypes.func.isRequired,
  getChapterList: PropTypes.func.isRequired,
};

RowItem.propTypes = {
  index: PropTypes.number.isRequired,
  setOrganization: PropTypes.func.isRequired,
  getChapterList: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    flex: 1,
    margin: 0,
    width: 80,
    height: 80,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ChooseOrg);
