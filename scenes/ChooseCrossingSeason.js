import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Picker, SafeAreaView, View } from 'react-native';
import { connect } from 'react-redux';
import { TextField } from 'react-native-material-textfield';
import * as actions from '../actions/registerActions';
import BottomButton from '../components/BottomButton';

class ChooseCrossingSeason extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentYear: moment().year(),
    };
    this.onChangeSeason = this.onChangeSeason.bind(this);
    this.onChangeYear = this.onChangeYear.bind(this);
    this.renderYearPicker = this.renderYearPicker.bind(this);
    this.onPressDone = this.onPressDone.bind(this);
    props.setCrossingSeason(
      props.crossingSeason.season,
      this.state.currentYear.toString()
    );
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Crossing Season',
    };
  };

  onChangeSeason(season) {
    this.props.setCrossingSeason(season, this.props.crossingSeason.year);
  }

  onChangeYear(year) {
    this.props.setCrossingSeason(this.props.crossingSeason.season, year);
  }

  onPressDone() {
    this.props.navigation.pop();
  }

  renderYearPicker() {
    var yearPickerItems = [];
    const yearFounded = parseInt(
      this.props.registration.organization.yearFounded
    );
    for (let i = this.state.currentYear; i >= yearFounded; i--) {
      const pickerItem = (
        <Picker.Item key={i} label={i.toString()} value={i.toString()} />
      );
      yearPickerItems.push(pickerItem);
    }
    return yearPickerItems;
  }

  render() {
    const seasons = ['Spring', 'Summer', 'Fall', 'Winter'];
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View
          style={{ flex: 3 / 2, flexDirection: 'row', alignItems: 'center' }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Picker
              style={{ flex: 1 / 2 }}
              selectedValue={this.props.crossingSeason.season}
              onValueChange={season => this.onChangeSeason(season)}
            >
              <Picker.Item label="Spring" value="Spring" />
              <Picker.Item label="Summer" value="Summer" />
              <Picker.Item label="Fall" value="Fall" />
              <Picker.Item label="Winter" value="Winter" />
            </Picker>
            <Picker
              style={{ flex: 1 / 2 }}
              selectedValue={this.props.crossingSeason.year}
              onValueChange={year => this.onChangeYear(year)}
            >
              {this.renderYearPicker()}
            </Picker>
          </View>
          <BottomButton onPress={this.onPressDone}>Done</BottomButton>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  registration: state.registration,
  crossingSeason: state.registration.crossingSeason,
});

function mapDispatchToProps(dispatch) {
  return {
    setCrossingSeason: (season, year) =>
      dispatch(actions.setCrossingSeason(season, year)),
  };
}

ChooseCrossingSeason.propTypes = {
  registration: PropTypes.shape({
    organization: PropTypes.shape({
      yearFounded: PropTypes.string.isRequired,
    }),
  }).isRequired,
  crossingSeason: PropTypes.shape({
    season: PropTypes.string.isRequired,
    year: PropTypes.string.isRequired,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  ChooseCrossingSeason
);
