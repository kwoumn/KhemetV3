import React from 'react';
import { ActivityIndicator, FlatList, StyleSheet, View } from 'react-native';
import { ListItem } from 'react-native-elements';
import { connect } from 'react-redux';
import * as chatActions from '../actions/chatActions';
import * as userActions from '../actions/userActions';
import Heading from '../components/text/Heading';

class MessageList extends React.Component {
  constructor() {
    super();
    this.convertUserChannelsObjToArr = this.convertUserChannelsObjToArr.bind(
      this
    );
  }

  static navigationOptions = () => {
    return {
      headerTitle: 'Messages',
    };
  };

  convertUserChannelsObjToArr(userChannels) {
    return Object.keys(userChannels)
      .map(channelSid => {
        return userChannels[channelSid];
      })
      .sort(function(a, b) {
        return a.dateUpdated > b.dateUpdated
          ? -1
          : b.dateUpdated > a.dateUpdated ? 1 : 0;
      });
  }

  render() {
    const { navigation, userChannels } = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          data={this.convertUserChannelsObjToArr(userChannels)}
          extraData={userChannels}
          renderItem={({ item, index }) => (
            <ListItem
              title={item.members}
              subtitle={item.lastMessage}
              key={index}
              onPress={() =>
                navigation.navigate('chat', {
                  authorUid: item.authorUid,
                  recipientUid: item.recipientUid,
                  channelSid: item.channelSid,
                  recipientName: item.members,
                })
              }
              hideChevron={true}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

const mapStateToProps = state => ({
  authorUid: state.user.user.uid,
  userChannels: state.messages.userChannels,
});

export default connect(mapStateToProps)(MessageList);
