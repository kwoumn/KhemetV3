import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import { ListItem } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { VISIBILITY_TYPE } from '../api/constants';
import { NavigationActions, StackActions } from 'react-navigation';
import * as locationActions from '../actions/locationActions';
import * as userActions from '../actions/userActions';

class Settings extends React.Component {
  constructor() {
    super();
    this.signOut = this.signOut.bind(this);
    this.onVisibilityToggle = this.onVisibilityToggle.bind(this);
    this.postGhostModeEvent = this.postGhostModeEvent.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Settings',
    };
  };

  signOut() {
    this.props.signOut().then(() => {
      this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'loginOrRegister' }),
          ],
        })
      );
    });
  }

  onVisibilityToggle(currentUserUid, shouldBeVisible) {
    this.props.updateVisibility(currentUserUid, shouldBeVisible).then(() => {
      if (shouldBeVisible === false) {
        this.postGhostModeEvent();
      }
    });
  }

  postGhostModeEvent() {
    const { authenticatedUser } = this.props;
    const eventType = 'ghost_mode';
    const userProperties = {
      organization: authenticatedUser.organization,
      chapter: authenticatedUser.chapter,
      school: authenticatedUser.school,
      crossingSeason: authenticatedUser.crossingSeason,
      crossingYear: authenticatedUser.crossingYear,
      lineNumber: authenticatedUser.lineNumber,
    };
    this.props.postAmplitudeEvent(
      eventType,
      this.props.currentUserUid,
      userProperties
    );
  }

  render() {
    const { visibilty, currentUserUid } = this.props;
    const isVisible = visibilty === VISIBILITY_TYPE.ON;
    return (
      <View style={styles.container}>
        <ListItem
          key="visibility"
          title="Share Location On Map"
          hideChevron={true}
          switchButton
          switched={isVisible}
          onSwitch={() => this.onVisibilityToggle(currentUserUid, !isVisible)}
        />
        <ListItem
          key="signOut"
          title="Sign Out"
          rightIcon={
            <Icon name="logout" size={30} style={{ paddingRight: 10 }} />
          }
          onPress={this.signOut}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

const mapStateToProps = state => ({
  currentUserUid: state.user.user.uid,
  visibilty: state.location.visibilityType,
  authenticatedUser: state.user.authenticatedUser,
});

function mapDispatchToProps(dispatch) {
  return {
    updateVisibility: (uid, shouldBeVisible) =>
      dispatch(locationActions.updateVisibility(uid, shouldBeVisible)),
    signOut: () => dispatch(userActions.signOut()),
    postAmplitudeEvent: (
      eventType,
      userId,
      userProperties,
      eventProperties,
      queryParams
    ) =>
      dispatch(
        userActions.postAmplitudeEvent(
          eventType,
          userId,
          userProperties,
          eventProperties,
          queryParams
        )
      ),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
