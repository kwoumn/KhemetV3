import React from 'react';
import Expo from 'expo';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  ActivityIndicator,
  Alert,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import Logo from '../components/Logo';
import Button from '../components/Button';
import AlertStatus from '../components/AlertStatus';
import { auth, database, provider } from '../api/firebase';
import { FACEBOOK_APP_ID } from '../api/firebaseConstants';
import * as actions from '../actions/registerActions';
import * as userActions from '../actions/userActions';
import * as chatActions from '../actions/chatActions';

class LoginOrRegister extends React.Component {
  constructor() {
    super();
    this.navigateToSignInPage = this.navigateToSignInPage.bind(this);
    this.navigateToRegisterPage = this.navigateToRegisterPage.bind(this);
    this.signInWithFacebook = this.signInWithFacebook.bind(this);
    this.navigateToRegisterPageFromSignInWithFacebook = this.navigateToRegisterPageFromSignInWithFacebook.bind(
      this
    );
    this.navigateToGreekMap = this.navigateToGreekMap.bind(this);
    this.state = {
      isLoaded: false,
    };
  }

  componentDidMount() {
    this.props.getSignedInUser().then(user => {
      this.setState({ isLoaded: true });
      if (user) {
        this.navigateToGreekMap();
      }
    });
  }

  navigateToSignInPage() {
    this.props.navigation.navigate('login');
  }

  navigateToRegisterPage() {
    this.props.navigation.navigate('register');
  }

  navigateToRegisterPageFromSignInWithFacebook(firebaseUser) {
    this.props.navigation.navigate('register', {
      signInWithFacebook: true,
      firebaseUser,
    });
  }

  navigateToGreekMap() {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'greekMap' })],
      })
    );
  }

  signInWithFacebook() {
    Expo.Facebook.logInWithReadPermissionsAsync(FACEBOOK_APP_ID, {
      permissions: ['email'],
    }).then(({ type, token }) => {
      if (type === 'success') {
        const credential = provider.credential(token);
        this.props.setLoading(false);
        auth
          .signInWithCredential(credential)
          .then(firebaseUser => {
            this.props.setUser(firebaseUser);
            this.props.createChatClient(firebaseUser.uid);
            const usersRef = database.ref(`users/${firebaseUser.uid}`);
            usersRef.once('value', dataSnapshot => {
              if (dataSnapshot.val() !== null) {
                this.props.navigation.replace('greekMap');
              } else {
                this.navigateToRegisterPageFromSignInWithFacebook(firebaseUser);
              }
            });
          })
          .catch(error => {
            Alert.alert(error.message);
          })
          .finally(() => {
            this.props.setLoading(true);
          });
      }
    });
  }

  render() {
    return !this.props.isLoaded || !this.state.isLoaded ? (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    ) : (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={loginStyle.loginContainer}>
          <Logo />
          <Button
            marginTop={20}
            onPress={this.navigateToSignInPage}
            width={300}
          >
            Sign in
          </Button>
          <View style={{ alignItems: 'center' }}>
            <Text style={loginStyle.text}>or sign in with:</Text>
          </View>
          <Button
            marginTop={20}
            onPress={this.signInWithFacebook}
            backgroundColor="#3b5998"
            width={350}
          >
            Login with Facebook
          </Button>
          <AlertStatus
            style={{ position: 'relative' }}
            textHelper="Don't have account?"
            textAction="Sign up"
            onPressAction={this.navigateToRegisterPage}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  isLoaded: state.registration.isLoaded,
});

function mapDispatchToProps(dispatch) {
  return {
    setLoading: isLoaded => dispatch(actions.setLoading(isLoaded)),
    setUser: user => dispatch(userActions.setUser(user)),
    createChatClient: uid => dispatch(chatActions.createChatClient(uid)),
    getSignedInUser: () => dispatch(userActions.getSignedInUser()),
  };
}

LoginOrRegister.propTypes = {
  isLoaded: PropTypes.bool.isRequired,
  setLoading: PropTypes.func.isRequired,
};

const loginStyle = StyleSheet.create({
  loginContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    paddingTop: 69,
  },
  text: {
    color: '#050409',
    marginTop: 15,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginOrRegister);
