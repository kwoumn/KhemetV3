import React from 'react';
import PropTypes from 'prop-types';
import {
  Keyboard,
  View,
  SafeAreaView,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import { connect } from 'react-redux';
import BottomButton from '../components/BottomButton';
import { TextField } from 'react-native-material-textfield';
import * as actions from '../actions/registerActions';

class ChooseLineNumber extends React.Component {
  constructor() {
    super();
    this.onSelectDone = this.onSelectDone.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Line Number',
    };
  };

  onSelectDone() {
    this.props.navigation.pop();
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.container}>
            <TextField
              label="Input Line Number"
              keyboardType="numeric"
              value={this.props.registration.lineNumber}
              onChangeText={lineNumber => this.props.setLineNumber(lineNumber)}
            />
            <BottomButton onPress={this.onSelectDone}>Done</BottomButton>
          </View>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  registration: state.registration,
});

function mapDispatchToProps(dispatch) {
  return {
    setLineNumber: lineNumber => dispatch(actions.setLineNumber(lineNumber)),
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
});

ChooseLineNumber.propTypes = {
  registration: PropTypes.shape({
    lineNumber: PropTypes.string,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChooseLineNumber);
