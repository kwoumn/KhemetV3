import React from 'react';
import {
  ActivityIndicator,
  Alert,
  Keyboard,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import PropTypes from 'prop-types';
import { TextField } from 'react-native-material-textfield';
import Heading from '../components/text/Heading';
import Button from '../components/Button';
import { getPlatformValue } from '../utils';
import { connect } from 'react-redux';
import { auth } from '../api/firebase';
import * as actions from '../actions/registerActions';
import * as userActions from '../actions/userActions';
import * as chatActions from '../actions/chatActions';

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
    };
    this.loginWithEmailAndPassword = this.loginWithEmailAndPassword.bind(this);
  }

  loginWithEmailAndPassword() {
    this.props.setLoading(false);
    auth
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(firebaseUser => {
        this.props.setUser(firebaseUser);
        this.props.createChatClient(firebaseUser.uid);
        this.props.navigation.dispatch(
          StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'greekMap' })],
          })
        );
      })
      .catch(error => {
        Alert.alert(error.message);
      })
      .finally(() => {
        this.props.setLoading(true);
      });
  }

  render() {
    return !this.props.isLoaded ? (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    ) : (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={loginStyle.loginContainer}>
          <Heading marginTop={16} color="#050409" textAlign="center">
            Sign In
          </Heading>
          <View style={loginStyle.formContainer}>
            <TextField
              label="Email"
              value={this.state.email}
              onChangeText={email => this.setState({ email })}
              keyboardType="email-address"
              onBlur={Keyboard.dismiss}
            />
            <TextField
              label="Password"
              value={this.state.password}
              onChangeText={password => this.setState({ password })}
              secureTextEntry
              onBlur={Keyboard.dismiss}
            />
            <Button marginTop={60} onPress={this.loginWithEmailAndPassword}>
              Sign in
            </Button>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = state => ({
  isLoaded: state.registration.isLoaded,
});

function mapDispatchToProps(dispatch) {
  return {
    setLoading: isLoaded => dispatch(actions.setLoading(isLoaded)),
    setUser: user => dispatch(userActions.setUser(user)),
    createChatClient: uid => dispatch(chatActions.createChatClient(uid)),
  };
}

Login.propTypes = {
  isLoaded: PropTypes.bool.isRequired,
  setLoading: PropTypes.func.isRequired,
};

const loginStyle = StyleSheet.create({
  loginContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    paddingTop: 49,
  },
  formContainer: {
    paddingLeft: 15,
    paddingRight: 15,
    marginTop: getPlatformValue('android', 25, 45),
  },
  icon: {
    color: '#050409',
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
