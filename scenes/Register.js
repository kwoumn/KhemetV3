import React from 'react';
import {
  ActivityIndicator,
  Alert,
  Image,
  Keyboard,
  ScrollView,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions, StackActions } from 'react-navigation';
import PropTypes from 'prop-types';
import { Avatar, Card, ListItem } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { ImagePicker, Permissions } from 'expo';
import Picker from 'react-native-picker';
import Button from '../components/Button';
import Heading from '../components/text/Heading';
import { getPlatformValue } from '../utils';
import { getOrgIcon } from '../utils/org';
import validate from 'validate.js';
import retry from 'async-retry';
import { auth, database } from '../api/firebase';
import { VISIBILITY_TYPE } from '../api/constants';
import * as actions from '../actions/registerActions';
import * as userActions from '../actions/userActions';
import * as locationActions from '../actions/locationActions';

export class Register extends React.Component {
  constructor() {
    super();
    this.state = {
      firstName: '',
      lastName: '',
      emailAddress: '',
      password: '',
      confirmPassword: '',
      constraints: {
        firstName: {
          presence: { allowEmpty: false },
        },
        lastName: {
          presence: { allowEmpty: false },
        },
        emailAddress: {
          presence: { allowEmpty: false },
          email: true,
        },
        password: {
          presence: true,
          length: {
            minimum: 6,
          },
          equality: 'confirmPassword',
        },
        organization: {
          presence: { allowEmpty: false },
        },
        chapter: {
          presence: { allowEmpty: false },
        },
        crossingSeason: {
          presence: { allowEmpty: false },
        },
        crossingYear: {
          presence: { allowEmpty: false },
        },
        school: {
          presence: { allowEmpty: false },
        },
        lineNumber: {
          presence: { allowEmpty: false },
        },
        occupation: {
          presence: { allowEmpty: false },
        },
        profilePhoto: {
          presence: { allowEmpty: false, message: 'is required.' },
        },
      },
      signInWithFacebookConstraints: {
        firstName: {
          presence: { allowEmpty: false },
        },
        lastName: {
          presence: { allowEmpty: false },
        },
        organization: {
          presence: { allowEmpty: false },
        },
        chapter: {
          presence: { allowEmpty: false },
        },
        crossingSeason: {
          presence: { allowEmpty: false },
        },
        crossingYear: {
          presence: { allowEmpty: false },
        },
        school: {
          presence: { allowEmpty: false },
        },
        lineNumber: {
          presence: { allowEmpty: false },
        },
        occupation: {
          presence: { allowEmpty: false },
        },
        profilePhoto: {
          presence: { allowEmpty: false, message: 'is required.' },
        },
      },
    };
    this.onPressChooseOrganization = this.onPressChooseOrganization.bind(this);
    this.handleChangeInput = this.handleChangeInput.bind(this);
    this.handleShowSeasonAndYearPicker = this.handleShowSeasonAndYearPicker.bind(
      this
    );
    this.onPressChooseChapter = this.onPressChooseChapter.bind(this);
    this.onPressChooseSchool = this.onPressChooseSchool.bind(this);
    this.getShield = this.getShield.bind(this);
    this.onPressLineNumber = this.onPressLineNumber.bind(this);
    this.onPressOccupation = this.onPressOccupation.bind(this);
    this.handleRegister = this.handleRegister.bind(this);
    this.writeUserDataOnSignup = this.writeUserDataOnSignup.bind(this);
    this.registerFromFacebook = this.registerFromFacebook.bind(this);
    this.registerWithEmailAndPassword = this.registerWithEmailAndPassword.bind(
      this
    );
    this.focusNextField = this.focusNextField.bind(this);
    this.blurField = this.blurField.bind(this);
    this.inputs = {};
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Create New Account',
    };
  };

  componentDidMount() {
    // Set default visibilty to on in redux store
    this.props.setVisibility(VISIBILITY_TYPE.ON);
  }

  onPressChooseOrganization() {
    this.props.navigation.navigate('chooseOrg');
  }

  focusNextField(id) {
    this.inputs[id].focus();
  }

  blurField(id) {
    this.inputs[id].blur();
  }

  onPressChooseChapter() {
    if (this.props.registration.organization) {
      this.props.navigation.navigate('chooseChapter');
    } else {
      Alert.alert('Please select your organization first.');
    }
  }

  onPressChooseSchool() {
    this.props.navigation.navigate('chooseSchool');
  }

  onPressLineNumber() {
    this.props.navigation.navigate('chooseLineNumber');
  }

  onPressOccupation() {
    this.props.navigation.navigate('inputOccupation');
  }

  handleChangeInput(stateName, text) {
    this.setState({
      [stateName]: text,
    });
  }

  handleShowSeasonAndYearPicker() {
    if (this.props.registration.organization) {
      this.props.navigation.navigate('chooseCrossingSeason');
    } else {
      Alert.alert('Please select your organization first.');
    }
  }

  getShield() {
    return (
      <Image
        source={getOrgIcon(this.props.registration.organization.name)}
        style={{ marginLeft: 10, width: 30, height: 30 }}
      />
    );
  }

  handleRegister() {
    if (this.props.navigation.getParam('signInWithFacebook', false) === true) {
      this.registerFromFacebook();
    } else {
      this.registerWithEmailAndPassword();
    }
  }

  registerFromFacebook() {
    const isValidationError = validate(
      {
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        emailAddress: this.props.navigation.getParam('firebaseUser', null)
          .email,
        organization: this.props.registration.organization,
        chapter: this.props.registration.chapter,
        school: this.props.registration.school,
        lineNumber: this.props.registration.lineNumber,
        occupation: this.props.registration.occupation,
        crossingSeason: this.props.registration.crossingSeason.season,
        crossingYear: this.props.registration.crossingSeason.year,
        profilePhoto: this.state.profilePhotoUri,
      },
      this.state.signInWithFacebookConstraints
    );
    if (isValidationError) {
      const validationMessages = Object.keys(isValidationError).map(key => {
        return isValidationError[key][0];
      });
      Alert.alert(validationMessages[0]);
    } else {
      this.props.setLoading(false);
      this.writeUserDataOnSignup(this.props.firebaseUser)
        .then(() => {
          this.props.navigation.dispatch(
            StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: 'greekMap' })],
            })
          );
        })
        .catch(error => {
          Alert.alert(error.message);
        })
        .finally(() => this.props.setLoading(true));
    }
  }

  registerWithEmailAndPassword() {
    const isValidationError = validate(
      {
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        emailAddress: this.state.emailAddress,
        password: this.state.password,
        confirmPassword: this.state.confirmPassword,
        organization: this.props.registration.organization,
        chapter: this.props.registration.chapter,
        school: this.props.registration.school,
        lineNumber: this.props.registration.lineNumber,
        occupation: this.props.registration.occupation,
        crossingSeason: this.props.registration.crossingSeason.season,
        crossingYear: this.props.registration.crossingSeason.year,
        profilePhoto: this.state.profilePhotoUri,
      },
      this.state.constraints
    );
    if (isValidationError) {
      const validationMessages = Object.keys(isValidationError).map(key => {
        return isValidationError[key][0];
      });
      Alert.alert(validationMessages[0]);
    } else {
      this.props.setLoading(false);
      auth
        .createUserWithEmailAndPassword(
          this.state.emailAddress,
          this.state.password
        )
        .then(firebaseUser => {
          this.props.setUser(firebaseUser);
          this.writeUserDataOnSignup(firebaseUser)
            .then(() => {
              this.props.navigation.dispatch(
                StackActions.reset({
                  index: 0,
                  actions: [
                    NavigationActions.navigate({ routeName: 'greekMap' }),
                  ],
                })
              );
            })
            .catch(error => {
              Alert.alert(error.message);
            });
        })
        .catch(error => {
          Alert.alert(error.message);
        })
        .finally(() => {
          this.props.setLoading(true);
        });
    }
  }

  writeUserDataOnSignup(firebaseUser) {
    const signInWithFacebook = this.props.navigation.getParam(
      'signInWithFacebook',
      false
    );
    !this.state.profilePhotoUri &&
      this.setState({
        profilePhotoUri:
          'https://firebasestorage.googleapis.com/v0/b/khemetv3.appspot.com/o/noprofilephoto.jpg?alt=media&token=3b726c49-4c04-4f2d-8a55-34582555e1cc',
      });
    if (signInWithFacebook) {
      const firebaseUserFB = this.props.navigation.getParam(
        'firebaseUser',
        null
      );
      return retry(bail => {
        return this.props
          .uploadProfilePhoto(this.state.profilePhotoUri, firebaseUserFB.uid)
          .then(profilePhotoUrl => {
            database.ref(`users/${firebaseUserFB.uid}`).set({
              firstName: this.state.firstName,
              lastName: this.state.lastName,
              emailAddress: firebaseUserFB.email,
              organization: this.props.registration.organization.name,
              chapter: this.props.registration.chapter,
              school: this.props.registration.school,
              lineNumber: this.props.registration.lineNumber,
              occupation: this.props.registration.occupation,
              crossingSeason: this.props.registration.crossingSeason.season,
              crossingYear: this.props.registration.crossingSeason.year,
              profilePhotoUrl,
            });
          });
      }).then(() => {
        return retry(bail => {
          database.ref(`locations/${firebaseUserFB.uid}`).update({
            visibilityType: this.props.visibility,
          });
        });
      });
    } else {
      return retry(bail => {
        return this.props.uploadProfilePhoto(
          this.state.profilePhotoUri,
          firebaseUser.uid
        );
      }).then(profilePhotoUrl => {
        return retry(bail => {
          return database.ref(`users/${firebaseUser.uid}`).set({
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            emailAddress: this.state.emailAddress,
            organization: this.props.registration.organization.name,
            chapter: this.props.registration.chapter,
            school: this.props.registration.school,
            lineNumber: this.props.registration.lineNumber,
            occupation: this.props.registration.occupation,
            crossingSeason: this.props.registration.crossingSeason.season,
            crossingYear: this.props.registration.crossingSeason.year,
            profilePhotoUrl,
          });
        }).then(() => {
          return retry(bail => {
            return database.ref(`locations/${firebaseUser.uid}`).set({
              visibilityType: this.props.visibility,
            });
          });
        });
      });
    }
  }

  _pickImage = async () => {
    const { status } = await Permissions.askAsync(
      Permissions.CAMERA,
      Permissions.CAMERA_ROLL
    );
    if (status === 'granted') {
      let pickerResult = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3],
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
      });
      this._handleImagePicked(pickerResult);
    } else {
      Alert.alert(
        'Please grant Khemet permissions to your camera and camera roll'
      );
    }
  };

  _handleImagePicked = pickerResult => {
    if (!pickerResult.cancelled) {
      this.setState({ profilePhotoUri: pickerResult.uri });
    }
  };

  render() {
    const {
      chapter,
      school,
      lineNumber,
      crossingSeason,
      occupation,
    } = this.props.registration;
    const signInWithFacebook = this.props.navigation.getParam(
      'signInWithFacebook',
      false
    );
    return !this.props.isLoaded ? (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    ) : (
      <KeyboardAwareScrollView enableResetScrollToCoords={false}>
        <ScrollView style={styles.loginContainer}>
          <View style={styles.formContainer}>
            <View
              style={{
                alignItems: 'center',
                marginTop: 10,
                justifyContent: 'center',
              }}
            >
              <Avatar
                xlarge
                rounded
                title="+"
                onPress={this._pickImage}
                activeOpacity={0.7}
                source={
                  this.state.profilePhotoUri && {
                    isStatic: true,
                    uri: this.state.profilePhotoUri,
                  }
                }
              />
              <Text style={styles.otherText} onPress={this._pickImage}>
                Add Photo
              </Text>
            </View>
            <Card>
              <TextField
                label="First Name"
                value={this.state.firstName}
                onChangeText={text => this.handleChangeInput('firstName', text)}
                returnKeyType="next"
                blurOnSubmit={false}
                ref={input => {
                  this.inputs['one'] = input;
                }}
                onSubmitEditing={() => {
                  this.blurField('one');
                  this.focusNextField('two');
                }}
              />
              <TextField
                label="Last Name"
                value={this.state.lastName}
                onChangeText={text => this.handleChangeInput('lastName', text)}
                returnKeyType="next"
                blurOnSubmit={false}
                ref={input => {
                  this.inputs['two'] = input;
                }}
                onSubmitEditing={() => {
                  this.blurField('two');
                  if (!signInWithFacebook) {
                    this.focusNextField('three');
                  }
                }}
              />
              {!signInWithFacebook && (
                <View>
                  <TextField
                    label="Email"
                    value={this.state.emailAddress}
                    onChangeText={text =>
                      this.handleChangeInput('emailAddress', text)
                    }
                    keyboardType="email-address"
                    returnKeyType="next"
                    blurOnSubmit={false}
                    ref={input => {
                      this.inputs['three'] = input;
                    }}
                    onSubmitEditing={() => {
                      this.blurField('three');
                      this.focusNextField('four');
                    }}
                  />
                  <TextField
                    label="Password"
                    value={this.state.password}
                    onChangeText={text =>
                      this.handleChangeInput('password', text)
                    }
                    secureTextEntry
                    returnKeyType="next"
                    ref={input => {
                      this.inputs['four'] = input;
                    }}
                    onSubmitEditing={() => {
                      this.blurField('four');
                      this.focusNextField('five');
                    }}
                  />
                  <TextField
                    label="Confirm Password"
                    value={this.state.confirmPassword}
                    onChangeText={text =>
                      this.handleChangeInput('confirmPassword', text)
                    }
                    secureTextEntry
                    returnKeyType="done"
                    ref={input => {
                      this.inputs['five'] = input;
                    }}
                    onSubmitEditing={Keyboard.dismiss}
                  />
                </View>
              )}
            </Card>
            <Card containerStyle={{ paddingLeft: 0 }}>
              <View style={{ marginLeft: -10, marginRight: -10 }}>
                <ListItem
                  style={styles.listItem}
                  title="Select Your Organization"
                  subtitle={
                    this.props.registration.organization
                      ? this.getShield()
                      : 'Tap to Select Organization'
                  }
                  onPress={this.onPressChooseOrganization}
                />
                <ListItem
                  style={styles.listItem}
                  title="Select Your Chapter"
                  subtitle={chapter !== '' ? chapter : 'Tap to Select Chapter'}
                  onPress={this.onPressChooseChapter}
                />
                <ListItem
                  style={styles.listItem}
                  title="What Season & Year Did You Cross?"
                  subtitle={`${crossingSeason.season} ${crossingSeason.year}`}
                  onPress={this.handleShowSeasonAndYearPicker}
                />
                <ListItem
                  style={styles.listItem}
                  title="Select Your School"
                  subtitle={school !== '' ? school : 'Tap to Select School'}
                  onPress={this.onPressChooseSchool}
                />
                <ListItem
                  style={styles.listItem}
                  title="Line Number"
                  subtitle={
                    lineNumber ? lineNumber : 'Tap to Input Line Number'
                  }
                  onPress={this.onPressLineNumber}
                />
                <ListItem
                  style={styles.listItem}
                  title="Occupation"
                  subtitle={occupation ? occupation : 'Tap to Input Occupation'}
                  onPress={this.onPressOccupation}
                />
              </View>
            </Card>
            <Button
              marginTop={getPlatformValue('android', 25, 38)}
              width={200}
              onPress={this.handleRegister}
            >
              Create
            </Button>
          </View>
        </ScrollView>
      </KeyboardAwareScrollView>
    );
  }
}

const mapStateToProps = state => ({
  registration: state.registration,
  isLoaded: state.registration.isLoaded,
  visibility: state.location.visibilityType,
});

function mapDispatchToProps(dispatch) {
  return {
    setLoading: isLoaded => dispatch(actions.setLoading(isLoaded)),
    setUser: user => dispatch(userActions.setUser(user)),
    setVisibility: visibility =>
      dispatch(locationActions.setVisibility(visibility)),
    uploadProfilePhoto: (photoUri, uid) =>
      dispatch(userActions.uploadProfilePhoto(photoUri, uid)),
  };
}

Register.propTypes = {
  registration: PropTypes.shape({
    organization: PropTypes.shape({
      name: PropTypes.string.isRequired,
      yearFounded: PropTypes.string.isRequired,
    }),
  }).isRequired,
  isLoaded: PropTypes.bool,
  setLoading: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  loginContainer: {
    flex: 1,
    paddingBottom: 30,
    marginBottom: 50,
    backgroundColor: 'transparent',
    paddingTop: getPlatformValue('android', 10, 30),
  },
  formContainer: {
    flex: 1,
    marginTop: getPlatformValue('android', 5, 34),
    marginBottom: 50,
  },
  icon: {
    color: '#050409',
  },
  listItem: {
    margin: 0,
    padding: 0,
  },
  otherText: {
    color: '#000000',
    fontSize: 18,
    paddingTop: 8,
    paddingBottom: 8,
    textAlign: 'center',
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
