import twilio from 'twilio';
import {
  TWILIO_ACCOUNT_SID,
  TWILIO_API_KEY,
  TWILIO_API_SECRET,
  TWILIO_IPM_SERVICE_SID,
} from '../api/twilioConstants';

const AccessToken = twilio.jwt.AccessToken;
const IpMessagingGrant = AccessToken.ChatGrant;

export function TokenGenerator(identity, deviceId) {
  const appName = 'Khemet';

  // Create a unique ID for the client on their current device
  const endpointId = appName + ':' + identity + ':' + deviceId;

  // Create a "grant" which enables a client to use IPM as a given user,
  // on a given device
  const ipmGrant = new IpMessagingGrant({
    serviceSid: TWILIO_IPM_SERVICE_SID,
    endpointId: endpointId,
  });

  // Create an access token which we will sign and return to the client,
  // containing the grant we just created
  const token = new AccessToken(
    TWILIO_ACCOUNT_SID,
    TWILIO_API_KEY,
    TWILIO_API_SECRET
  );

  token.addGrant(ipmGrant);
  token.identity = identity;

  return token;
}
