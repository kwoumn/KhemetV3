import { TokenGenerator } from './tokenService';

var chatClient;

function fetchAccessToken(userId) {
  const token = TokenGenerator(userId, 'mobile').toJwt();
  connectMessagingClient(token);
}

function connectMessagingClient(token) {
  Twilio.Chat.Client.create(token).then(client => {
    chatClient = client;
  });
}

function createChannel() {
  chatClient
    .createChannel({
      uniqueName: generateUUID(uuidType),
      friendlyName: 'Test Channel',
    })
    .then(channel => {
      console.log('Channel Created');
      console.log(channel);
    });
}

function generateUUID(uuidType) {
  var d = new Date().getTime();
  if (
    typeof performance !== 'undefined' &&
    typeof performance.now === 'function'
  ) {
    d += performance.now();
  }
  let uuidStr = 'SCxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
  return uuidStr.replace(/[xy]/g, function(c) {
    var r = ((d + Math.random() * 16) % 16) | 0;
    d = Math.floor(d / 16);
    return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
  });
}
