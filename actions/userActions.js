import axios from 'axios';
import queryString from 'query-string';
import * as types from './actionTypes';
import { auth, database, storage } from '../api/firebase';
import { dispatch } from 'react-redux';
import { Platform } from 'react-native';
import retry from 'async-retry';
import { serverBaseURI } from '../api/constants';
import { getLocationsToDisplay } from './locationActions';

export function setUser(user) {
  return {
    type: types.SET_USER,
    data: {
      user,
    },
  };
}

export function setSelectedUser(selectedUser) {
  return {
    type: types.SET_SELECTED_USER,
    data: {
      selectedUser,
    },
  };
}

export function setIsProfileModalVisible(isProfileModalVisible) {
  return {
    type: types.SET_IS_PROFILE_MODAL_VISIBLE,
    data: {
      isProfileModalVisible,
    },
  };
}

export function getFirstNameFromUid(uid) {
  return dispatch => {
    return database
      .ref(`users/${uid}/firstName`)
      .once('value', firstNameSnapshot => {
        if (firstNameSnapshot.exists()) {
          return firstNameSnapshot.val();
        } else {
          return 'Inactive Account';
        }
      });
  };
}

export function getAuthenticatedUserAttributes(uid) {
  return dispatch => {
    return new Promise(function(resolve, reject) {
      return database
        .ref(`users/${uid}`)
        .on('value', userAttributesSnapshot => {
          const userAttributes = {
            firstName: userAttributesSnapshot.val().firstName,
            organization: userAttributesSnapshot.val().organization,
            chapter: userAttributesSnapshot.val().chapter,
            school: userAttributesSnapshot.val().school,
            crossingSeason: userAttributesSnapshot.val().crossingSeason,
            crossingYear: userAttributesSnapshot.val().crossingYear,
            lineNumber: userAttributesSnapshot.val().lineNumber,
            occupation: userAttributesSnapshot.val().occupation,
            profilePhotoUrl: userAttributesSnapshot.val().profilePhotoUrl,
          };
          dispatch(setAuthenticatedUserAttributes(userAttributes));
          resolve(userAttributes);
        });
    });
  };
}

export function setAuthenticatedUserAttributes(userAttributes) {
  return {
    type: types.SET_AUTHENTICATED_USER_ATTRIBUTES,
    data: {
      userAttributes,
    },
  };
}

export function uploadProfilePhoto(photoUri, uid) {
  return dispatch => {
    const ref = storage.ref('profilePhotos').child(`${uid}`);
    return fetch(photoUri).then(resp => {
      return resp.blob().then(blob => {
        return ref.put(blob).then(snapshot => {
          return snapshot.downloadURL;
        });
      });
    });
  };
}

export function signOut() {
  return dispatch => {
    return auth.signOut();
  };
}

export function getSignedInUser() {
  return dispatch => {
    return new Promise(function(resolve, reject) {
      auth.onAuthStateChanged(user => {
        if (user) {
          dispatch(setUser(user));
          resolve(user); //User successfully logged in
        } else {
          resolve(null); //User not logged in
        }
      });
    });
  };
}

export function postAmplitudeEvent(
  eventType,
  userId,
  userProperties,
  eventProperties,
  queryParams
) {
  return dispatch => {
    return axios.post(
      `${serverBaseURI}/event/${eventType}/user/${userId}?${queryString.stringify(
        queryParams
      )}`,
      {
        userProperties,
        eventProperties,
      }
    );
  };
}

export function getBlockedUsers(currentUserUid) {
  return dispatch => {
    return retry(bail => {
      return database
        .ref(`users/${currentUserUid}/blocked`)
        .once('value', blockedUsersSnapshot => {
          const blockedUsers = blockedUsersSnapshot.val()
            ? blockedUsersSnapshot.val()
            : [];
          dispatch(setBlockedUsers(blockedUsers));
          return blockedUsers;
        });
    });
  };
}

export function getBlockedByUsers(currentUserUid) {
  return dispatch => {
    return retry(bail => {
      return database
        .ref(`users/${currentUserUid}/blockedBy`)
        .on('value', blockedByUsersSnapshot => {
          const blockedByUsers = blockedByUsersSnapshot.val()
            ? blockedByUsersSnapshot.val()
            : [];
          dispatch(setBlockedByUsers(blockedByUsers));
          dispatch(getLocationsToDisplay());
          return blockedByUsers;
        });
    });
  };
}

export function blockUser(currentUserUid, blockedUserUid) {
  return (dispatch, getState) => {
    const currentlyBlockedUsers = getState().user.blocked;
    const newBlockedUsers = currentlyBlockedUsers.concat(blockedUserUid);
    return retry(bail => {
      return database.ref(`users/${currentUserUid}`).update({
        blocked: newBlockedUsers,
      });
    }).then(() => {
      dispatch(setBlockedUsers(newBlockedUsers));
      return retry(bail => {
        return database
          .ref(`users/${blockedUserUid}/blockedBy`)
          .once('value', blockedBySnapshot => {
            return blockedBySnapshot;
          });
      }).then(blockedBySnapshot => {
        const newBlockedBy = blockedBySnapshot.val()
          ? blockedBySnapshot.val().concat(currentUserUid)
          : [currentUserUid];
        return retry(bail => {
          return database.ref(`users/${blockedUserUid}`).update({
            blockedBy: newBlockedBy,
          });
        });
      });
    });
  };
}

export function unblockUser(currentUserUid, unblockedUserUid) {
  return (dispatch, getState) => {
    const currentlyBlockedUsers = getState().user.blocked;
    const newBlockedUsers = currentlyBlockedUsers.filter(
      uid => uid !== unblockedUserUid
    );
    return retry(bail => {
      return database.ref(`users/${currentUserUid}`).update({
        blocked: newBlockedUsers,
      });
    }).then(() => {
      dispatch(setBlockedUsers(newBlockedUsers));
      return retry(bail => {
        return database
          .ref(`users/${unblockedUserUid}/blockedBy`)
          .once('value', blockedBySnapshot => {
            return blockedBySnapshot;
          });
      }).then(blockedBySnapshot => {
        const newBlockedBy = blockedBySnapshot
          .val()
          .filter(uid => uid !== currentUserUid);
        return retry(bail => {
          return database.ref(`users/${unblockedUserUid}`).update({
            blockedBy: newBlockedBy,
          });
        });
      });
    });
  };
}

export function hasAgreedToTerms(currentUserUid) {
  return dispatch => {
    return new Promise(function(resolve, reject) {
      return retry(bail => {
        return database
          .ref(`users/${currentUserUid}/hasAgreedToTerms`)
          .on('value', hasAgreedToTermsSnapshot => {
            dispatch(setHasAgreedToTerms(hasAgreedToTermsSnapshot.val()));
            return resolve();
          });
      });
    });
  };
}

function setHasAgreedToTerms(hasAgreedToTerms) {
  return {
    type: types.SET_HAS_AGREED_TO_TERMS,
    data: {
      hasAgreedToTerms,
    },
  };
}

export function agreeToTerms(currentUserUid) {
  return dispatch => {
    return retry(bail => {
      return database
        .ref(`users/${currentUserUid}`)
        .update({ hasAgreedToTerms: true })
        .then(() => {
          return dispatch(setHasAgreedToTerms(true));
        });
    });
  };
}

export function reportProfile(reportingUserUid, offendingUserUid) {
  return dispatch => {
    const payload = {
      from: '"Khemet Profile Reporter" <info@khemet.com>',
      to: '"Kyle Woumn" <kyle@khemet.com>',
      subject: 'Profile Reported',
      text: `User ${offendingUserUid} has been reported by User ${reportingUserUid} for inappropriate content. Please investigate this report and take appropriate action.`,
      html: `<p>User ${offendingUserUid} has been reported by User ${reportingUserUid} for inappropriate content. Please investigate this report and take appropriate action.</p>`,
    };
    return axios.post(`${serverBaseURI}/report`, payload);
  };
}

export function reportConversation(reportingUserUid, channelSid) {
  return dispatch => {
    const payload = {
      from: '"Khemet Conversation Reporter" <info@khemet.com>',
      to: '"Kyle Woumn" <kyle@khemet.com>',
      subject: 'Conversation Reported',
      text: `User ${reportingUserUid} has reported conversation ${channelSid} for inappropriate content. Please investigate this report and take appropriate action.`,
      html: `<p>User ${reportingUserUid} has reported conversation ${channelSid} for inappropriate content. Please investigate this report and take appropriate action.</p>`,
    };
    return axios.post(`${serverBaseURI}/report`, payload);
  };
}

function setBlockedUsers(blockedUsers) {
  return {
    type: types.SET_BLOCKED_USERS,
    data: {
      blockedUsers,
    },
  };
}

function setBlockedByUsers(blockedByUsers) {
  return {
    type: types.SET_BLOCKED_BY_USERS,
    data: {
      blockedByUsers,
    },
  };
}
