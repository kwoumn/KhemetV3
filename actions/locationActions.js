import { dispatch } from 'react-redux';
import * as types from './actionTypes';
import { database } from '../api/firebase';
import { VISIBILITY_TYPE } from '../api/constants';
import retry from 'async-retry';

export function setVisibility(visibilityType) {
  return {
    type: types.SET_VISIBILITY,
    data: {
      visibilityType,
    },
  };
}

export function updateVisibility(uid, shouldBeVisible) {
  const visibilityType = shouldBeVisible
    ? VISIBILITY_TYPE.ON
    : VISIBILITY_TYPE.OFF;
  return dispatch => {
    return new Promise(function(resolve, reject) {
      return retry(bail => {
        return database
          .ref(`locations/${uid}`)
          .update({ visibilityType })
          .then(() => {
            dispatch(setVisibility(visibilityType));
            resolve();
          });
      });
    });
  };
}

export function storeLocation(latitude, longitude) {
  return (dispatch, getState) => {
    const uid = getState().user.user.uid;
    database
      .ref(`locations/${uid}/locationToDisplay`)
      .update({
        latitude,
        longitude,
      })
      .catch(error => {
        console.log(error);
      })
      .finally(() => {
        dispatch(setLocation(latitude, longitude, uid));
      });
  };
}

export function setLocation(latitude, longitude, uid) {
  return {
    type: types.SET_LOCATION,
    data: {
      latitude,
      longitude,
    },
  };
}

export function getLocationsToDisplay() {
  const locationMap = {};
  return (dispatch, getState) => {
    database.ref('locations').on('value', locationSnapshot => {
      locationSnapshot.forEach(location => {
        if (
          location.key !== getState().user.user.uid &&
          !getState().user.blockedBy.includes(location.key) &&
          location.val().locationToDisplay
        ) {
          locationMap[location.key] = {
            uid: location.key,
            latitude: location.val().locationToDisplay.latitude,
            longitude: location.val().locationToDisplay.longitude,
            visibilty: location.val().visibilityType,
          };
        }
      });
      const locations = Object.keys(locationMap).map(key => {
        return {
          uid: key,
          latitude: locationMap[key].latitude,
          longitude: locationMap[key].longitude,
          visibilty: locationMap[key].visibilty,
        };
      });
      dispatch(setLocationsToDisplay(locations));
    });
  };
}

export function getUserInfoFromUid(uid) {
  return dispatch => {
    return database
      .ref(`users/${uid}`)
      .once('value')
      .then(userSnapshot => {
        return userSnapshot.val();
      });
  };
}

export function getCurrentUserVisibility(uid) {
  return dispatch => {
    return database
      .ref(`locations/${uid}/visibilityType`)
      .on('value', visibilitySnapshot => {
        const visibility = visibilitySnapshot.val();
        dispatch(setVisibility(visibility));
        return visibility;
      });
  };
}

export function setLocationsToDisplay(locationsToDisplay) {
  return {
    type: types.SET_LOCATIONS_TO_DISPLAY,
    data: {
      locationsToDisplay,
    },
  };
}

export function onLocationAdded() {
  return dispatch => {
    return database.ref('locations').on('child_added', locationSnapshot => {
      dispatch(getLocationsToDisplay());
    });
  };
}

export function onLocationRemoved() {
  return dispatch => {
    return database.ref('locations').on('child_removed', locationSnapshot => {
      dispatch(getLocationsToDisplay());
    });
  };
}
