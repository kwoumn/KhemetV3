import { dispatch } from 'react-redux';
import axios from 'axios';
import moment from 'moment';
import { database } from '../api/firebase';
import chat from 'twilio-chat';
import * as types from './actionTypes';
import { getFirstNameFromUid, getSignedInUser } from './userActions';
import { serverBaseURI } from '../api/constants';
import { AccessManager } from 'twilio-common';

const MESSAGE_UUID = 'MESSAGE_UUID';
const SINGLE_CONVERSATION_UUID = 'SINGLE_CONVERSATION_UUID';
const GROUP_CONVERSATION_UUID = 'GROUP_CONVERSATION_UUID';

export function getToken(uid, pushChannel) {
  if (!pushChannel) {
    pushChannel = 'none';
  }
  return fetch(
    `${serverBaseURI}/token?identity=${uid}&pushChannel=${pushChannel}`
  ).then(response => {
    return response.text();
  });
}

export function createChatClient(uid, pushChannel) {
  return dispatch => {
    return getToken(uid, pushChannel).then(token => {
      const accessManager = new AccessManager(token);
      const chatClient = chat
        .create(token)
        .then(chatClient => {
          dispatch(updateToken(accessManager, chatClient));
          return chatClient;
        })
        .catch(error => console.error(error));
      return chatClient;
    });
  };
}

function updateToken(accessManager, chatClient) {
  return dispatch => {
    accessManager.on('tokenUpdated', function(am) {
      // get new token from AccessManager and pass it to the library instance
      chatClient.updateToken(am.token);
    });
  };
}

export function getChannelsForUser(authorUid) {
  return dispatch => {
    const userChannels = dispatch(createChatClient(authorUid))
      .then(chatClient => {
        dispatch(subscribeToNewMessages(chatClient));
        return chatClient.getUserChannelDescriptors();
      })
      .then(channelDescriptorPaginator => {
        return channelDescriptorPaginator.items.map(channelDescriptor => {
          return channelDescriptor.getChannel();
        });
      })
      .then(channelPromises => {
        return Promise.all(channelPromises).then(channels => {
          return channels;
        });
      })
      .then(channels => {
        return channels.filter(channel => {
          return channel.lastMessage !== undefined;
        });
      })
      .then(channels => {
        return channels.map(channel => {
          return channel.getMembers().then(members => {
            return dispatch(getMessages(channel)).then(messages => {
              return {
                members,
                messages,
                lastMessage: messages[0].text,
                channelSid: channel.sid,
                dateUpdated: moment(channel.lastMessage.timestamp).format(),
              };
            });
          });
        });
      })
      .then(memberMessageArrPromises => {
        return Promise.all(memberMessageArrPromises).then(memberMessageArr => {
          return memberMessageArr;
        });
      })
      .then(memberMessageArr => {
        return memberMessageArr.map(memberMessage => {
          const nonAuthorMember = memberMessage.members.find(member => {
            return member.identity !== authorUid;
          });
          const lastMessage = memberMessage.lastMessage;
          const channelSid = memberMessage.channelSid;
          const dateUpdated = memberMessage.dateUpdated;
          const messages = memberMessage.messages;
          return dispatch(getFirstNameFromUid(nonAuthorMember.identity)).then(
            name => {
              return {
                members: name.val() ? name.val() : 'Inactive User',
                lastMessage,
                messages,
                recipientUid: nonAuthorMember.identity,
                authorUid,
                channelSid,
                dateUpdated,
              };
            }
          );
        });
      })
      .then(memberMessagesByNamePromiseArr => {
        return Promise.all(memberMessagesByNamePromiseArr)
          .then(memberMessagesByName => {
            return memberMessagesByName;
          })
          .catch(err => {
            console.error(err);
          });
      })
      .then(filteredMemberMessages => {
        return filteredMemberMessages;
      })
      .then(userChannels => {
        return dispatch(setUserChannels(userChannels));
      })
      .catch(err => {
        console.error(err);
      });
  };
}

function getMessages(channel) {
  return dispatch => {
    return channel.getMessages().then(messagePaginator => {
      return messagePaginator.items.reverse().map(message => {
        return {
          _id: message.sid,
          text: message.body,
          createdAt: message.timestamp,
          user: {
            _id: message.author,
          },
        };
      });
    });
  };
}

function createMessagePayload(message) {
  return {
    _id: message.sid,
    text: message.body,
    createdAt: message.timestamp,
    user: {
      _id: message.author,
    },
  };
}

function getChannelBySid(chatClient, channelSid) {
  return dispatch => {
    return chatClient.getChannelBySid(channelSid).then(channel => channel);
  };
}

function getChannelMembers(channel) {
  return dispatch => {
    return channel.getMembers(members => members);
  };
}

function subscribeToNewMessages(chatClient) {
  return (dispatch, getState) => {
    chatClient.on('messageAdded', message => {
      if (getState().messages.userChannels[message.channel.sid]) {
        dispatch(
          updateUserChannels(
            createMessagePayload(message),
            message.body,
            message.channel.sid,
            moment(message.dateUpdated).format()
          )
        );
      } else {
        dispatch(getChannelBySid(chatClient, message.channel.sid))
          .then(channel => channel)
          .then(channel => dispatch(getChannelMembers(channel)))
          .then(channelMembers => {
            const currentUserUid = getState().user.user.uid;
            const recipientUid = channelMembers.find(
              member => member.identity !== currentUserUid
            ).identity;
            dispatch(getFirstNameFromUid(recipientUid)).then(name => {
              const members = name.val() ? name.val() : 'Inactive User';
              dispatch(setNewMessageChannel(message, members, recipientUid));
            });
          });
      }
    });
  };
}

export function setPushId(uid, pushId) {
  return dispatch => {
    database.ref(`users/${uid}`).update({ pushId });
    dispatch(setPushPlayerId(pushId));
  };
}

function setNewMessageChannel(message, members, recipientUid) {
  return {
    type: types.SET_NEW_MESSAGE_CHANNEL,
    data: {
      message,
      members,
      recipientUid,
    },
  };
}

function setUserChannels(userChannels) {
  return {
    type: types.SET_USER_CHANNELS,
    data: {
      userChannels,
    },
  };
}

export function setNewMessage(channelSid, messagePayload) {
  return {
    type: types.SET_NEW_MESSAGE,
    data: {
      channelSid,
      messagePayload,
    },
  };
}

function updateUserChannels(
  messagePayload,
  messageBody,
  channelSid,
  dateUpdated
) {
  return {
    type: types.UPDATE_USER_CHANNELS,
    data: {
      messagePayload,
      messageBody,
      channelSid,
      dateUpdated,
    },
  };
}

export function setPushPlayerId(pushPlayerId) {
  return {
    type: types.SET_PUSH_PLAYER_ID,
    data: {
      pushPlayerId,
    },
  };
}
