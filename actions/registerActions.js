import axios from 'axios';
import { dispatch } from 'react-redux';
import { getChapterUrl } from '../api/constants';
import * as types from './actionTypes';

export function setOrganization(organization) {
  return {
    type: types.SET_ORGANIZATION,
    data: {
      organization,
    },
  };
}

export function setChapterList(chapterList) {
  return {
    type: types.SET_CHAPTER_LIST,
    data: {
      chapterList,
    },
  };
}

export function setChapter(chapter) {
  return {
    type: types.SET_CHAPTER,
    data: {
      chapter,
    },
  };
}

export function setSchool(school) {
  return {
    type: types.SET_SCHOOL,
    data: {
      school,
    },
  };
}

export function setLineNumber(lineNumber) {
  return {
    type: types.SET_LINE_NUMBER,
    data: {
      lineNumber,
    },
  };
}

export function setOccupation(occupation) {
  return {
    type: types.SET_OCCUPATION,
    data: {
      occupation,
    },
  };
}

export function setCrossingSeason(season, year) {
  return {
    type: types.SET_CROSSING_SEASON,
    data: {
      season,
      year,
    },
  };
}

export function getChapterList(yearFounded) {
  const chapterURI = getChapterUrl(yearFounded);
  return dispatch => {
    axios
      .get(chapterURI)
      .then(({ data }) => {
        dispatch(setChapterList(data));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function setLoading(isLoaded) {
  return {
    type: types.SET_LOADING,
    data: {
      isLoaded,
    },
  };
}
